-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 23 août 2021 à 11:32
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `forum`
--

-- --------------------------------------------------------

--
-- Structure de la table `abonne`
--

CREATE TABLE `abonne` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `abonne`
--

INSERT INTO `abonne` (`id`, `id_user`, `email`, `password`) VALUES
(1, 2, 'deffokevin14@gmail.com', 'okay'),
(2, 3, 'kamga@gmail.com', 'kamg'),
(3, 4, 'ef2eere@dfdfd', 'efer'),
(4, 8, 'deffokevin1t@gmail.com', 'rmfrwfwe'),
(5, 9, 'deffokeviddd@gmail.com', 'ddwqdw'),
(6, 10, 'deffokevio@gmail.com', 'ilhhff');

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`id`, `id_user`, `email`, `password`) VALUES
(1, 1, 'superadmin@forum.com', 'adminforum');

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nom_cat` text NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modification` datetime DEFAULT NULL,
  `niveau` varchar(255) NOT NULL,
  `photo_cat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `id_user`, `nom_cat`, `date_creation`, `date_modification`, `niveau`, `photo_cat`) VALUES
(2, 1, 'quasar', '2021-08-05 21:51:07', NULL, '1', 'hills-2836301_1920.jpg05-08-2021a21-511.jpg'),
(3, 1, 'CSS', '2021-08-05 21:51:37', NULL, '1', 'istockphoto-947254500-612x612.jpg05-08-2021a21-511.jpg'),
(4, 1, 'JAVASCRIPT', '2021-08-05 21:53:40', NULL, '1', 'Tarif_d\'un_terrassier_au_m3_et_par_jour.jpg05-08-2021a21-531.jpg'),
(5, 1, 'JAVA', '2021-08-05 22:04:46', NULL, '1', '11.jpg05-08-2021a22-041.jpg'),
(6, 1, 'robo', '2021-08-06 15:11:58', '2007-08-00 08:57:08', '1', 'trello-board-invite-qr-code.png07-08-2021a20-57.png'),
(7, 1, 'PHPs', '2021-08-08 09:23:23', '2009-08-21 10:30:21', '1', 'LogoMakr-3OkVKT_(1).png09-08-2021a10-30.png'),
(8, 1, 'AJAX', '2021-08-10 00:53:10', NULL, '1', 'SequenceDeconnection.jpg10-08-2021a00-531.jpg'),
(9, 1, 'nimportequoi', '2021-08-10 16:41:42', NULL, '1', 'DESKSHOP.png10-08-2021a16-411.png'),
(11, 1, 'Html', '2021-08-10 23:03:45', NULL, '1', 'DESKSHOP_(1).png10-08-2021a23-031.png'),
(12, 2, 'laravel', '2021-08-13 00:43:44', NULL, '1', '1113-08-2021a00-4318.jpg'),
(13, 1, 'vujs', '2021-08-14 20:25:53', NULL, '1', '52ace17c53cd535578a5999d4266eb74.jpg14-08-2021a20-251.jpg'),
(14, 1, 'ht', '2021-08-18 11:41:10', NULL, '1', '5.png18-08-2021a11-411.png'),
(15, 1, 'AJAXs', '2021-08-19 12:27:40', '2019-08-21 12:55:07', '1', 'mountains-540115_1920.jpg19-08-2021a12-55.jpg'),
(16, 1, 'symfonie', '2021-08-19 23:11:11', NULL, '1', 'SequenceInscription.jpg19-08-2021a23-111.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE `commentaire` (
  `id` int(11) NOT NULL,
  `id_theme` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `contenu` text NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `statut` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commentaire`
--

INSERT INTO `commentaire` (`id`, `id_theme`, `id_user`, `contenu`, `date_creation`, `statut`) VALUES
(1, 12, 1, 'je sais meme ?', '2008-08-21 08:50:43', '1'),
(2, 7, 1, 'vas sur google et tu verra', '2008-08-21 08:52:34', '1'),
(3, 12, 1, 'b78h8787', '2009-08-21 10:28:48', '1'),
(4, 14, 1, 'cherche', '2009-08-21 01:57:51', '1'),
(5, 13, 1, 'je sais meme ?', '2009-08-21 01:58:14', '1'),
(6, 12, 1, 'okay', '2009-08-21 01:58:33', '1'),
(7, 11, 1, 'comment', '2009-08-21 01:58:53', '1'),
(8, 4, 1, 'en mettant', '2009-08-21 01:59:15', '1'),
(9, 10, 1, 'cite  alors', '2009-08-21 01:59:37', '1'),
(10, 11, 1, 'creer une classe', '2010-08-21 12:18:37', '1'),
(11, 9, 1, 'comme la boucle for', '2010-08-21 12:19:04', '1'),
(12, 9, 1, 'comme la boucle foreach', '2010-08-21 12:19:30', '1'),
(13, 9, 1, 'va regarder sur google', '2010-08-21 12:20:05', '1'),
(14, 12, 1, 'va regarder sur google', '2010-08-21 12:20:26', '1'),
(15, 12, 1, 'vas sur google et tu verra', '2010-08-21 12:20:39', '1'),
(16, 14, 1, 'va regarder sur google', '2010-08-21 12:20:50', '1'),
(17, 14, 1, 'vas sur google et tu verra', '2010-08-21 12:21:03', '1'),
(18, 15, 1, 'va regarder sur google', '2010-08-21 12:44:45', '1'),
(19, 15, 1, 'vas sur google et tu verra', '2010-08-21 12:45:02', '1'),
(20, 18, 1, 'vas sur google et tu verra', '2010-08-21 12:45:26', '1'),
(21, 18, 1, 'vas sur google et tu verra', '2010-08-21 12:45:45', '1'),
(22, 16, 1, 'okay', '2010-08-21 12:46:51', '1'),
(23, 6, 1, 'ok', '2010-08-21 12:47:03', '1'),
(24, 16, 1, 'va regarder sur google', '2010-08-21 12:48:00', '1'),
(25, 16, 1, 'vas sur google et tu verra', '2010-08-21 12:48:18', '1'),
(26, 19, 1, 'ajax sert a afficher du contenu sans toute fois rafraichir la page', '2010-08-21 12:55:05', '1'),
(27, 20, 1, 'pour ne pasetre obliger de rafraichir la page a chaque instant', '2010-08-21 12:56:26', '1'),
(28, 17, 1, 'essai voir', '2010-08-21 02:20:17', '1'),
(29, 2, 1, 'ok', '2010-08-21 02:20:44', '1'),
(31, 13, 1, 'non', '2010-08-21 03:54:16', '1'),
(44, 24, 2, 'njj', '2013-08-21 11:04:09', '1'),
(45, 12, 2, '1', '2013-08-21 03:19:20', '1'),
(46, 12, 2, '2', '2013-08-21 03:19:36', '1'),
(47, 12, 2, '3', '2013-08-21 03:19:50', '1'),
(48, 12, 2, '4', '2013-08-21 03:20:04', '1'),
(49, 12, 2, '42', '2013-08-21 03:20:18', '1'),
(50, 29, 2, 'for', '2018-08-21 01:21:39', '1'),
(51, 19, 2, 'wdhwwywtwtwdt', '2021-08-19 18:52:00', '1'),
(52, 32, 2, 'jkkwkfkw', '2021-08-20 11:21:00', '1'),
(53, 32, 2, 'jkkwkfkw', '2021-08-20 11:21:00', '1'),
(54, 32, 2, 'jkkwkfkw', '2021-08-20 11:21:00', '1'),
(55, 32, 2, 'jkkwkfkw', '2021-08-20 11:21:00', '1');

-- --------------------------------------------------------

--
-- Structure de la table `moderateur`
--

CREATE TABLE `moderateur` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `moderateur`
--

INSERT INTO `moderateur` (`id`, `id_user`, `email`, `password`) VALUES
(18, 2, 'deffokevin14@gmail.com', 'okay');

-- --------------------------------------------------------

--
-- Structure de la table `mod_admin`
--

CREATE TABLE `mod_admin` (
  `id` int(11) NOT NULL,
  `id_auteur` int(11) NOT NULL,
  `id_destinataire` int(11) NOT NULL,
  `contenu` varchar(255) NOT NULL,
  `dates` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `reaction`
--

CREATE TABLE `reaction` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_commentaire` int(11) NOT NULL,
  `niveau` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `reaction`
--

INSERT INTO `reaction` (`id`, `id_user`, `id_commentaire`, `niveau`) VALUES
(1, 2, 1, '1'),
(2, 2, 26, '1'),
(3, 2, 44, '1');

-- --------------------------------------------------------

--
-- Structure de la table `signalement`
--

CREATE TABLE `signalement` (
  `id` int(11) NOT NULL,
  `id_theme` int(11) DEFAULT NULL,
  `id_commentaire` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `choix` varchar(255) NOT NULL,
  `statut` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `theme`
--

CREATE TABLE `theme` (
  `id` int(11) NOT NULL,
  `id_cat` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `libelle` text NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modification` datetime DEFAULT NULL,
  `niveau` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `theme`
--

INSERT INTO `theme` (`id`, `id_cat`, `id_user`, `libelle`, `date_creation`, `date_modification`, `niveau`) VALUES
(2, 5, 1, 'essai', '2021-08-06 00:35:16', NULL, '1'),
(4, 3, 1, 'comment mettre les bordures', '2021-08-06 00:46:37', NULL, '1'),
(6, 5, 1, 'jqvqse', '2021-08-06 01:25:42', NULL, '1'),
(7, 4, 1, 'comment commencer', '2021-08-06 10:59:16', NULL, '1'),
(9, 7, 1, 'comment fontionne la boucle foreach ?', '2021-08-08 09:25:00', NULL, '1'),
(10, 7, 1, 'les frameworks du php', '2021-08-08 09:27:22', NULL, '1'),
(11, 3, 1, 'comment faire un fond degrade', '2021-08-08 09:28:54', NULL, '1'),
(12, 4, 1, 'c est quoi le DOM ?', '2021-08-08 09:30:47', NULL, '1'),
(13, 2, 1, 'c est qoui quasar ?', '2021-08-09 13:56:28', NULL, '4'),
(14, 2, 1, 'a quoi ca sert ?', '2021-08-09 13:56:56', NULL, '1'),
(15, 6, 1, 'oui robo', '2021-08-09 13:57:22', NULL, '1'),
(16, 6, 1, 'parlons de la technologie robo', '2021-08-10 00:22:10', NULL, '1'),
(17, 7, 1, 'comment ca marche', '2021-08-10 00:22:29', NULL, '1'),
(18, 5, 1, 'ouioui', '2021-08-10 00:24:01', NULL, '1'),
(19, 11, 1, 'A quoi sert ajax dans la programation?', '2021-08-10 00:54:11', '2011-08-21 12:45:39', '1'),
(20, 8, 1, 'pourqoi utiliser ajax ?', '2021-08-10 00:54:34', NULL, '1'),
(24, 11, 2, 'bob', '2021-08-12 06:48:47', NULL, '1'),
(29, 13, 2, 'okiu', '2021-08-17 13:59:12', NULL, '1'),
(32, 15, 1, 'cool', '2021-08-19 12:57:10', NULL, '1'),
(33, 16, 2, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>ok</p>\r\n</body>\r\n</html>', '2021-08-20 15:38:10', NULL, '1');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `couleur_prefere` varchar(255) NOT NULL,
  `meilleur_ami` varchar(255) NOT NULL,
  `profil` varchar(255) NOT NULL,
  `niveau` int(11) NOT NULL,
  `notif` varchar(255) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `prenom`, `couleur_prefere`, `meilleur_ami`, `profil`, `niveau`, `notif`) VALUES
(1, 'super', 'admin', 'noir', 'einstein', 'manager.jpg', 3, '1'),
(2, 'DEFFO', 'KEVIN', 'bleu', 'moi', 'DESKSHOP-removebg-preview060820211104.png', 2, '1'),
(3, 'kamga', 'paul', 'noir', 'serpent', 'pexels-furkanfdemir-6309853_(1)060820211106.jpg', 1, '1'),
(4, 'hiu', 'res', 'ed', 'efz', 'LogoMakr-3OkVKT_(1)070820210220.png', 1, '1'),
(5, 'DEFFO', 'KEVIN', 'bleu', 'rfefrefrrfrfrww', 'DESKSHOP190820211815.png', 1, '1'),
(6, 'DEFFO', 'KEVIN', 'bleu', 'rfefrefrrfrfrww', 'DESKSHOP190820211819.png', 1, '1'),
(7, 'DEFFO', 'KEVIN', 'bleu', 'rfefrefrrfrfrww', 'DESKSHOP190820211823.png', 1, '1'),
(8, 'DEFFO', 'KEVIN', 'bleu', 'rfefrefrrfrfrww', 'DESKSHOP190820211824.png', 1, '1'),
(9, 'qwdqw', 'qwddwq', 'wqdwd', 'wdwq', 'SequenceInscription200820211644.jpg', 1, '1'),
(10, 'ngano', 'hech', 'noir', 'moi', 'téléchargement_(6)200820211817.jpg', 1, '1');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `abonne`
--
ALTER TABLE `abonne`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `fk_id_user2` (`id_user`);

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `fk_id_user` (`id_user`);

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_user3` (`id_user`);

--
-- Index pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_user5` (`id_user`),
  ADD KEY `fk_id_theme` (`id_theme`);

--
-- Index pour la table `moderateur`
--
ALTER TABLE `moderateur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `fk_id_user1` (`id_user`);

--
-- Index pour la table `mod_admin`
--
ALTER TABLE `mod_admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_auteur` (`id_auteur`),
  ADD KEY `fk_id_destinataire` (`id_destinataire`);

--
-- Index pour la table `reaction`
--
ALTER TABLE `reaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_user7` (`id_user`),
  ADD KEY `fk_id_commentaire1` (`id_commentaire`);

--
-- Index pour la table `signalement`
--
ALTER TABLE `signalement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_user6` (`id_user`),
  ADD KEY `fk_id_theme1` (`id_theme`),
  ADD KEY `fk_id_commentaire` (`id_commentaire`);

--
-- Index pour la table `theme`
--
ALTER TABLE `theme`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_user4` (`id_user`),
  ADD KEY `fk_id_cat` (`id_cat`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `abonne`
--
ALTER TABLE `abonne`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT pour la table `commentaire`
--
ALTER TABLE `commentaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT pour la table `moderateur`
--
ALTER TABLE `moderateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT pour la table `mod_admin`
--
ALTER TABLE `mod_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `reaction`
--
ALTER TABLE `reaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `signalement`
--
ALTER TABLE `signalement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `theme`
--
ALTER TABLE `theme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `abonne`
--
ALTER TABLE `abonne`
  ADD CONSTRAINT `fk_id_user2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `fk_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD CONSTRAINT `fk_id_user3` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD CONSTRAINT `fk_id_theme` FOREIGN KEY (`id_theme`) REFERENCES `theme` (`id`),
  ADD CONSTRAINT `fk_id_user5` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `moderateur`
--
ALTER TABLE `moderateur`
  ADD CONSTRAINT `fk_id_user1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `mod_admin`
--
ALTER TABLE `mod_admin`
  ADD CONSTRAINT `fk_id_auteur` FOREIGN KEY (`id_auteur`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `fk_id_destinataire` FOREIGN KEY (`id_destinataire`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `reaction`
--
ALTER TABLE `reaction`
  ADD CONSTRAINT `fk_id_commentaire1` FOREIGN KEY (`id_commentaire`) REFERENCES `commentaire` (`id`),
  ADD CONSTRAINT `fk_id_user7` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `signalement`
--
ALTER TABLE `signalement`
  ADD CONSTRAINT `fk_id_commentaire` FOREIGN KEY (`id_commentaire`) REFERENCES `commentaire` (`id`),
  ADD CONSTRAINT `fk_id_theme1` FOREIGN KEY (`id_theme`) REFERENCES `theme` (`id`),
  ADD CONSTRAINT `fk_id_user6` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `theme`
--
ALTER TABLE `theme`
  ADD CONSTRAINT `fk_id_cat` FOREIGN KEY (`id_cat`) REFERENCES `categorie` (`id`),
  ADD CONSTRAINT `fk_id_user4` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
