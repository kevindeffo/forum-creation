<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	class Abonne extends CI_Controller {

		public function index(){
			if (isset($_SESSION['Abonne'])) {
				$data['allcat']=$this->Categorie->findAllCategorieBd();
				$this->load->view('Welcome/Bienvenueheader');
				$this->load->view('Visiteurs/hello',$data);
				$this->load->view('Welcome/Bienvenuefooter');
			} else{
				session_destroy();
				redirect(site_url(array('Welcome','index')));
			}
		}
		// Ajoute un Abonne en BD //
		public function AddAbonne(){
		// print_r($_FILES);  
				if ( isset($_POST) ) {
					// print_r($_POST);
					$etat = $this->testExitAbonne( $_POST['email'] );
					if ( $etat==0 ) { 
						if (isset($_FILES['profil']) AND $_FILES['profil']['error'] == 0 ){
							//Testons si le fichier n'est pas trop gros
							if ($_FILES['profil']['size'] <= 100000000){ // 8 zero
									// Testons si l'extension est autorisée
									$infosfichier = pathinfo($_FILES['profil']['name']);
									$extension_upload = $infosfichier['extension'];

									$config = $infosfichier['filename'].date('d').''.date('m').''.date('Y').''.date('H').''.date('i');
									$ma_variable = str_replace('.', '_', $config);
									$ma_variable = str_replace(' ', '_', $config);
									$config = $ma_variable.'.'.$extension_upload;
									move_uploaded_file($_FILES['profil']['tmp_name'],'assets/images/user_profil/'.$config);
									$data2['profil'] = $config;

									$data2['nom'] = $_POST['nom'];
									$data2['prenom'] = $_POST['prenom'];
									$data2['notif'] = $_POST['notif'];
									$data2['niveau'] = $_POST['niveau'];
									$data2['couleur_prefere'] = $_POST['couleur_prefere'];
									$data2['meilleur_ami'] = $_POST['meilleur_ami'];

									$this->User->hydrate($data2);
									$this->User->AddUser($data2);

									$lastuser = $this->User->findLastUser();
									$data1['id_user'] = $lastuser['user']['id'];
									$data1['email'] = $_POST['email'];
									$data1['password'] = $_POST['password'];
									//print_r($data1);
								
									$this->Abonne->hydrate($data1);
									$this->Abonne->AddAbonne($data1);

									$_SESSION['message_save']=" Votre Compte a ete Créé avec success !!";
									$_SESSION['success']='ok';
									redirect(site_url(array('Abonne','formulaireconnexion')));
				
							}else{
									$_SESSION['message_save']="La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
									$_SESSION['message']='non';
									redirect(site_url(array('Welcome','inscription')));
							}
						}else{
							$_SESSION['message_save']="L'image choisie  est endommagée  veuillez la remplacer svp !!";
							$_SESSION['message']='non';
							redirect(site_url(array('Welcome','inscription')));
						}

						
						
					} else{
							redirect(site_url(array('Welcome','inscription')));
					}
				} else{
			  	redirect(site_url(array('Welcome','inscription')));
				 }
		}

		// verifie si l'Abonne qu'il faut inserer existe deja en BD
		// et retourne 1 si oui et 0 sinon.
		public function testExitAbonne($email){   
			$etat = 0;
			$data['infoAbonne'] = $this->Abonne->findAllAbonne();
			if ($data['infoAbonne']['total'] <= 0) {
				
			}else{
				for ( $i=0; $i < $data['infoAbonne']['total'] ; $i++) { 
					if ($data['infoAbonne'][$i]['email'] == $email) {
						$etat = 1;
						break;
					}else{
						$etat = 0;
					}
				}
			}
			return $etat;
		}	

		// renvoie vers le formulaire de connexion
		public function formulaireconnexion() {
			$this->load->view('USER/formulaireconnexion');
		}

		// gestion connexion des users
		public function traitementConnexion(){
			if ( isset($_POST['email']) && isset($_POST['password']) ){
				$_SESSION['mail'] = $_POST['email'];
				$_SESSION['pswd'] = $_POST['password'];
				$Abonne = $this->Abonne->findAllAbonne(); //print_r($Abonne);
				for ( $i=0; $i < $Abonne['total']; $i++ ) {
				  $niveau[$i] = $this->User->finduserInfos($Abonne[$i]['id_user']);
				} //print_r($niveau);
				for ( $i=0; $i < $Abonne['total']; $i++ ) { 
					if ( $Abonne[$i]['email'] == $_POST['email'] && $Abonne[$i]['password'] == $_POST['password'] &&  $niveau[$i]['niveau'] == 1  ) {
							$cible = $Abonne[$i]['id_user'];
							$cordonnees = $this->User->finduserInfos($cible);
							$_SESSION['Abonne']['nom'] = $cordonnees['nom'];
							$_SESSION['Abonne']['id'] = $cordonnees['id'];
							$_SESSION['Abonne']['prenom'] = $cordonnees['prenom'] ;
							$_SESSION['Abonne']['profil'] = $cordonnees['profil'];
							$_SESSION['Abonne']['notif'] = $cordonnees['notif'];
					}
				}
				
				if (isset($_SESSION['Abonne'])) {
					redirect(site_url(array('Abonne','index')));
				}else{
					redirect(site_url(array('Moderateur','traitementConnexion')));
				}
			}
			else{
				redirect(site_url(array('Abonne','formulaireconnexion')));
			}
		}

		// fonction pour deconnecter un Abonne
		public function deconnexion(){
			if (isset($_SESSION['Abonne'])) {
				session_destroy();
			}
			redirect(site_url(array('Abonne','index')));
		}

		public function detail_categories(){
			$this->load->view('USER/det_cat');
			
		}

		// lerob: j'ai commente ceci car il etait question qu'apres  l'ajout d'un commentaire, on puisse revenir
		//  sur la page du commentaire
		// public function ajoutCommentaire(){
		// 	if (isset($_SESSION['Abonne'])) {
		// 		if (isset($_POST) AND !empty($_POST)) {
		// 			$data['contenu']=$_POST['comment'];
		// 			$data['id_theme']=$_POST['id_theme'];
		// 			$data['date_creation']=$_POST['date_creation'];
		// 			$data['statut']=$_POST['niveau'];
		// 			$data['id_user']=$_POST['id_user'];
		// 			$this->Commentaire->hydrate($data);
		// 			$this->Commentaire->addCommentaire();
		// 			redirect(site_url(array('Welcome','liste_commentairetheme')));
		// 		}else{
		// 			redirect(site_url(array('Welcome','formulaireConnexion')));
		// 		}
		// 	}
		// }


	}


