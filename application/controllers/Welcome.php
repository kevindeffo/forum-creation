<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Welcome extends CI_Controller {

		public function index(){
			$this->load->view('Welcome/Bienvenueheader');
			$this->load->view('Welcome/Bienvenuebody');
			$this->load->view('Welcome/Bienvenuefooter');
		}

		public function inscription(){

			$this->load->view('USER/formulaire_inscription');
			
		}


		public function liste_theme(){
			if (isset($_POST) AND !empty($_POST)) {
				$data['themes']= $this->Theme->findThemeinbd($_POST['id_cat']); 
				if ($data['themes']['total']!=0) {
					for ($i=0; $i <$data['themes']['total'] ; $i++) { 

						$comment[$i]= $this->Commentaire->findCommentaire($data['themes'][$i]['id']);
					}
				} else {
					$data['themes'][0] = ' Cette categorie ne contient encore aucun theme ';
					$comment[0]['contenu'] = ' Ce Theme ne contient encore aucun Commentaire ';
				}
				$data['allcategorie']=$_POST['nom_cat'];
				$data['comment']=$comment;
				$data['photo_cat'] = $_POST['photo_cat'];
				$this->load->view('Welcome/Bienvenueheader');
				$this->load->view('Visiteurs/categories_theme',$data);
				$this->load->view('Welcome/Bienvenuefooter');
			}else{echo "jjj";}
		}


		public function liste_commentairetheme(){ 
			if (isset($_POST)) {

				if ($_POST['Selecteur'] == 1) {
					$data['comment']=$this->Commentaire->findCommentaire($_POST['id_theme']);
					$data['categorie']=$this->Categorie->findcategorieInfos($_POST['id_cat']);
					$data['theme']=$this->Theme->findThemeinfo($_POST['id_theme']);
					$data['id_cat'] = $_POST['id_cat'];
					$data['reaction']=$this->Reaction->findAllReaction();
					// print_r($data['reaction']);
					// echo '</br></br></br>';
					// print_r($data['comment']);
					for ($i=0; $i < count($data['comment'])-2; $i++) { 
							$data['createur'][$i] = $this->User->finduserInfos($data['comment'][$i]['id_user']);
					}
				}

				if ($_POST['Selecteur'] == 2) {
					if (isset($_SESSION['Abonne'])) {
						if (isset($_POST) AND !empty($_POST)) {
							$donnees['contenu']=$_POST['comment'];
							$donnees['id_theme']=$_POST['id_theme'];
							$donnees['date_creation']=$_POST['date_creation'];
							$donnees['statut']=$_POST['niveau'];
							$donnees['id_user']=$_POST['id_user'];
							$this->Commentaire->hydrate($donnees);
							$this->Commentaire->addCommentaire();
							$data['comment']=$this->Commentaire->findCommentaire($_POST['id_theme']);
							$data['categorie']=$this->Categorie->findcategorieInfos($_POST['id_cat']);
							$data['theme']=$this->Theme->findThemeinfo($_POST['id_theme']);
							$data['id_cat'] = $_POST['id_cat'];
							$data['reaction']=$this->Reaction->findAllReaction();
							for ($i=0; $i < count($data['comment'])-2; $i++) { 
									$data['createur'][$i] = $this->User->finduserInfos($data['comment'][$i]['id_user']);
							}
						}else{
							redirect(site_url(array('Welcome','formulaireConnexion')));
						}
					}
				}

					$this->load->view('Welcome/Bienvenueheader');
					$this->load->view('WELCOME/visit_theme',$data);
					$this->load->view('Welcome/Bienvenuefooter');
			}
					// print_r($_POST);
			

		}

		// fonction pour signaler un message .........{ajout}............
		// je dois recevoir ici :
		// id du commentaire
		// id de la session en cours
		// id du themes
		// statut en hidden vaut 1 pour non lu et 2 pour lu  
		public function SignalerComment(){
			if ( isset($_SESSION['Abonne'])) {
				if ( isset($_POST) ) {

					$destinataire = $this->Abonne->findabonneInfos($_POST['id']);
					$donnees['id_user'] = $destinataire['id_user'];
					$donnees['id_commentaire'] = $_POST['id_comment'];
					$donnees['id_theme'] = $_POST['id_theme'];
					$donnees['choix'] = 1;
					$donnees['statut'] = 1;
					// print_r($data);
					$this->Signalement->hydrate($donnees);
					$this->Signalement->InsertionSignal($donnees);
					$data['comment']=$this->Commentaire->findCommentaire($_POST['id_theme']);
					$data['categorie']=$this->Categorie->findcategorieInfos($_POST['id_cat']);
					$data['theme']=$this->Theme->findThemeinfo($_POST['id_theme']);
					$data['id_cat'] = $_POST['id_cat'];
					$data['reaction']=$this->Reaction->findAllReaction();
					for ($i=0; $i < count($data['comment'])-2; $i++) { 
							$data['createur'][$i] = $this->User->finduserInfos($data['comment'][$i]['id_user']);
					}
					$this->load->view('Welcome/Bienvenueheader');
					$this->load->view('WELCOME/visit_theme',$data);
					$this->load->view('Welcome/Bienvenuefooter');

				} else{
					$_SESSION['ERROR'] = 'erreur nous ne sommes pas parvenu a signaler ce message ! veillez reassayer .';
					redirect(site_url(array('Welcome','forum')));
				  }
			} else {
				$_SESSION['message'] = ' Vous devez vous connecter afin d\'interagir sur le forum ';
				redirect(site_url(array('Abonne','formulaireconnexion')));
			  }
		}


		// fonction pour aimer un message .........{ajout}............
		// je dois recevoir ici :
		// id du commentaire
		// id de la session en cours
		// statut en hidden vaut 1 pour j'aime 
		public function LikeComment(){
			if ( isset($_SESSION['Abonne'])) {
				if ( isset($_POST) ) {
					
					$donnees['id_user'] = $_POST['id_user'];
					$donnees['id_commentaire'] = $_POST['id_comment'];
					$donnees['niveau'] = 1;
					$this->Reaction->hydrate($donnees);
					$this->Reaction->Insertion();
					$data['comment']=$this->Commentaire->findCommentaire($_POST['id_theme']);
					$data['categorie']=$this->Categorie->findcategorieInfos($_POST['id_cat']);
					$data['theme']=$this->Theme->findThemeinfo($_POST['id_theme']);
					$data['id_cat'] = $_POST['id_cat'];
					$data['reaction']=$this->Reaction->findAllReaction();
					for ($i=0; $i < count($data['comment'])-2; $i++) { 
							$data['createur'][$i] = $this->User->finduserInfos($data['comment'][$i]['id_user']);
					}
					$this->load->view('Welcome/Bienvenueheader');
					$this->load->view('WELCOME/visit_theme',$data);
					$this->load->view('Welcome/Bienvenuefooter');

				} else{
					$_SESSION['ERROR'] = 'impossible d\'effectuer cette action ! veillez reassayer .';
					redirect(site_url(array('Welcome','forum')));
				  }
			} else {
				$_SESSION['message'] = ' Vous devez vous connecter afin d\'interagir sur le forum ';
				redirect(site_url(array('Abonne','formulaireconnexion')));
			  }
		}



		// fonction pour aimer un message .........{ajout}............
		// je dois recevoir ici :
		// id du commentaire
		// id de la session en cours
		// statut en hidden vaut 2 pour je n'aime pas 
		public function UnLikeComment(){
			if ( isset($_SESSION['Abonne'])) {
				if ( isset($_POST) ) {
					
					$donnees['id_user'] = $_POST['id_user'];
					$donnees['id_commentaire'] = $_POST['id_comment'];
					$donnees['niveau'] = 2;
					$this->Reaction->hydrate($donnees);
					$this->Reaction->Insertion();
					$data['comment']=$this->Commentaire->findCommentaire($_POST['id_theme']);
					$data['categorie']=$this->Categorie->findcategorieInfos($_POST['id_cat']);
					$data['theme']=$this->Theme->findThemeinfo($_POST['id_theme']);
					$data['id_cat'] = $_POST['id_cat'];
					$data['reaction']=$this->Reaction->findAllReaction();
					for ($i=0; $i < count($data['comment'])-2; $i++) { 
							$data['createur'][$i] = $this->User->finduserInfos($data['comment'][$i]['id_user']);
					}
					$this->load->view('Welcome/Bienvenueheader');
					$this->load->view('WELCOME/visit_theme',$data);
					$this->load->view('Welcome/Bienvenuefooter');

				} else{
					$_SESSION['ERROR'] = 'impossible d\'effectuer cette action ! veillez reassayer .';
					redirect(site_url(array('Welcome','forum')));
				  }
			} else {
				$_SESSION['message'] = ' Vous devez vous connecter afin d\'interagir sur le forum ';
				redirect(site_url(array('Abonne','formulaireconnexion')));
			  }
		}


		public function forum(){
			$data['allcat']=$this->Categorie->findAllCategorieBd();
			$this->load->view('Welcome/Bienvenueheader');
			$this->load->view('Visiteurs/hello',$data);
			$this->load->view('Welcome/Bienvenuefooter');
		}

		
	}
?>