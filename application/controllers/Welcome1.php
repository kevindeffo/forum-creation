<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Welcome extends CI_Controller {

		public function index()
		{
			$data['allcategorie']=$this->Categorie->findAllCategorieBd();
			$this->load->view('WELCOME/header');
			$this->load->view('WELCOME/hello',$data);
			$this->load->view('WELCOME/footer');
		}

		public function inscription(){

			$this->load->view('USER/formulaire_inscription');
			
		}


		public function liste_theme(){
			if (isset($_POST)) {
				$a=$_POST['id_cat'];
				$data['themes']= $this->Theme->findThemeinbd($_POST['id_cat']); 
				print_r($_POST);
				print_r($data['themes']);
				for ($i=0; $i <$data['themes']['total'] ; $i++) { 
					$comment[$i]= $this->Commentaire->findCommentaire($data['themes'][$i]['id']);
					
				}
				$data['allcategorie']=$_POST['nom_cat'];
				$data['comment']=$comment;
				$data['photo_cat'] = $_POST['photo_cat'];
				$this->load->view('WELCOME/header');
				$this->load->view('WELCOME/categories_theme',$data);
				$this->load->view('WELCOME/footer');

			}
		}


		public function liste_commentairetheme(){
			if (isset($_POST)) {
				$data['comment']=$this->Commentaire->findCommentaire($_POST['id_theme']);
				$data['categorie']=$this->Categorie->findcategorieInfos($_POST['id_cat']);
				$data['theme']=$this->Theme->findThemeinfo($_POST['id_theme']);
				$this->load->view('WELCOME/visit_theme',$data);
			}
		}

		// fonction pour signaler un message .........{ajout}............
		// je dois recevoir ici :
		// id du commentaire
		// id de la session en cours
		// id du themes
		// statut en hidden vaut 1 pour non lu    et 2 pour lu  
		public function SignalerComment(){
			if ( isset($_SESSION['Abonne'])) {
				if ( isset($_POST) ) {

					$destinataire = $this->Abonne->findabonneInfos($_POST['id']);
					$data['id_user'] = $destinataire['id_user'];
					$data['id_commentaire'] = $_POST['id_comment'];
					$data['id_theme'] = $_POST['id_theme'];
					$data['choix'] = 1;
					$data['statut'] = 1;
					print_r($data);
					$this->Signalement->hydrate($data);
					$this->Signalement->InsertionSignal($data);

				} else{
					$_SESSION['ERROR'] = 'erreur nous ne sommes pas parvenu a signaler ce message ! veillez reassayer .';
				  }
			} else {
				$_SESSION['message'] = ' Vous devez vous connecter afin d\'interagir sur le forum ';
				redirect(site_url(array('Abonne','formulaireconnexion')));
			  }
		}


		// fonction pour aimer un message .........{ajout}............
		// je dois recevoir ici :
		// id du commentaire
		// id de la session en cours
		// statut en hidden vaut 1 pour j'aime 
		public function LikeComment(){
			if ( isset($_SESSION['Abonne'])) {
				if ( isset($_POST) ) {
					
					$data['id_user'] = $_POST['id_user'];
					$data['id_commentaire'] = $_POST['id_comment'];
					$data['niveau'] = 1;
					$this->Reaction->hydrate($data);
					$this->Reaction->Insertion($data);

				} else{
					$_SESSION['ERROR'] = 'impossible d\'effectuer cette action ! veillez reassayer .';
				  }
			} else {
				$_SESSION['message'] = ' Vous devez vous connecter afin d\'interagir sur le forum ';
				redirect(site_url(array('Abonne','formulaireconnexion')));
			  }
		}



		// fonction pour aimer un message .........{ajout}............
		// je dois recevoir ici :
		// id du commentaire
		// id de la session en cours
		// statut en hidden vaut 2 pour je n'aime pas 
		public function UnLikeComment(){
			if ( isset($_SESSION['Abonne'])) {
				if ( isset($_POST) ) {
					
					$data['id_user'] = $_POST['id_user'];
					$data['id_commentaire'] = $_POST['id_comment'];
					$data['niveau'] = 2;
					$this->Reaction->hydrate($data);
					$this->Reaction->Insertion($data);

				} else{
					$_SESSION['ERROR'] = 'impossible d\'effectuer cette action ! veillez reassayer .';
				  }
			} else {
				$_SESSION['message'] = ' Vous devez vous connecter afin d\'interagir sur le forum ';
				redirect(site_url(array('Abonne','formulaireconnexion')));
			  }
		}

		
	}
?>