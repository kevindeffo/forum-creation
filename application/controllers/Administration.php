<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administration extends CI_Controller {
	
	public function index(){
		
		 if (isset($_SESSION['ADMIN'])) {
		 	$allcategorie=$this->Categorie->findAllCategoriemodel();
			$data['allnotifcategorie']=$allcategorie;

			$allTheme=$this->Theme->findAllThememodel();
			$data['allnotiftheme']=$allTheme;
			$data['alluser']= $this->User->findAllUsersBd();
			$data['allTheme']= $this->Theme->findAllThemeinbd();
			$data['allContenuCommentaire']= $this->Commentaire->findallcommentairebd();
			$this->load->view('ADMIN/index',$data);
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/home');
			$this->load->view('ADMIN/footer');
		
				
		}else{
	   	 session_destroy();
		 redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}
	 		
	
	// fonction qui charge le formulaire de connexion pour un administrateur
	public function formulaireConnexion(){
		
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_SESSION['ADMIN'])) {
				redirect(site_url(array('Administration','index')));
			}else{
				session_destroy();
				redirect(site_url(array('Administration','formulaireConnexion')));
			}
		}else{
			$this->load->view('gestion_admin/formulaire_connexion');
		}
	}


	// Gestions des Administrateurs


	public function manageAdmin(){
		
		if (isset($_SESSION['ADMIN'])) {
			
			$data['AllAdmin']=$this->Admin->findAllAdminBd();
			$this->load->view('WELCOME/index',$data);
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/home_admin');
			$this->load->view('WELCOME/footer');
			
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}
	public function testExitAdmin($email){
        $etat=0;
        $data['infoAdmin']=$this->Admin->findAllAdminBd();
        if ($data['infoAdmin']['total']<=0) {
            
        }else{
            for ($i=0; $i <$data['infoAdmin']['total'] ; $i++) { 
                if ($data['infoAdmin'][$i]['email']==$email) {
                    $etat=1;
                    break;
                }else{
                    $etat=0;
                }
            }
        }
        return $etat;
	}


	// Fonction qui permet d ajouter une categorie
 		

	public function addAdmin(){
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST)) {
				$etat=$this->testExitAdmin($_POST['email']);
				if ($etat==0) {
					if (isset($_FILES['profil']) AND $_FILES['profil']['error'] == 0 ){
	        		// Testons si le fichier n'est pas trop gros
	                    if ($_FILES['profil']['size'] <= 100000000){
	                        // Testons si l'extension est autorisée
	                        $infosfichier =pathinfo($_FILES['profil']['name']);
	                        $extension_upload = $infosfichier['extension'];

	                        $config =$_FILES['profil']['name'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i').$_SESSION['ADMIN']['id'];
	 						$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable.'.'.$extension_upload;
							move_uploaded_file($_FILES['profil']['tmp_name'],'assets/images/user_profil/'.$config);
							$data['profil']=$config;
							
	                    }else{
	                        $data['message_save']="La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
	                        $data['message']='non';
	                    }
	                }else{
	                    $data['message_save']="L'image choisie  est endommagée  veuillez le remplacer svp !!";
	                    $data['message']='non';
	                }
					
					$data['nom']=$_POST['nom'];
					$data['email']=$_POST['email'];
					$data['password']=$_POST['password'];
					$data['telephone']=$_POST['telephone'];
					
					$data['date']=date('Y-m-d H:i:s');
					$this->Admin->hydrate($data);
					$this->Admin->addAdmin();
					$_SESSION['message_save']="Administrateurs enregistré avec success !!";
			 		$_SESSION['success']='ok';
			 		redirect(site_url(array('Administration','manageAdmin')));
					
				}else{
					session_destroy();
					direct(site_url(array('Administration','formulaireConnexion')));
				}
			}else{
				session_destroy();
				redirect(site_url(array('Administration','formulaireConnexion')));
			}
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}

	}


	public function manageConnexion(){
		
		if (isset($_POST['email']) && isset($_POST['password'])) {
			$admin = $this->Admin->findAllAdminBd();
			for ($i=0; $i < $admin['total']; $i++) { 
				 if ($admin[$i]['email'] == $_POST['email'] && $admin[$i]['password'] == $_POST['password']) {
				 	$a=$admin[$i]['id_user'];
				 	$_SESSION['ADMIN'] = $admin[$i] ;
				 	$cord=$this->User->finduserInfos($a);
					$etat=1;
					
				}else{
					$etat=0;
				}
		 	}
			
			if ($etat==1) {
				$_SESSION['ADMIN']['nom'] = $cord['nom'].' '.$cord['prenom'] ;
				$_SESSION['ADMIN']['prenom'] = $cord['prenom'] ;
				$_SESSION['ADMIN']['nom'] = $cord['nom'];
				$_SESSION['ADMIN']['profil'] = $cord['profil'];
				redirect(site_url(array('Administration','index')));
			}else{
				$_SESSION['ERR'] = 'Les paramtres recus ne correspondent a auccun adminstrateur dans notre Database.<br> <b>Veillez recommencer SVP</b>';
				redirect(site_url(array('Administration','formulaireConnexion')));
			}
		}
		else{
			redirect(site_url(array('Administration','formulaireConnexion')));
			
		}
	}


	public function deconnexion(){
		if (isset($_SESSION['ADMIN'])) {
			session_destroy();
		}
		redirect(site_url(array('Administration','index')));
	}




     //  ajout Kevin Deffo
	public function formulaireAddTheme(){
		if(isset($_SESSION['ADMIN'])){
			$Tcate = $this->Categorie->findAllCategorieBd();
			$data['nom']=$Tcate;
			$allTheme=$this->Theme->findAllThememodel();
			$data['allnotiftheme']=$allTheme;
			$data['alluser']= $this->User->findAllUsersBd();
			$data['allTheme']= $this->Theme->findAllThemeinbd();
			 $this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('formulaireAddTheme',$data);
			$this->load->view('ADMIN/footer');
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}
	public function formulaireAddCat(){
		if(isset($_SESSION['ADMIN'])){
			$allTheme=$this->Theme->findAllThememodel();
			$data['allnotiftheme']=$allTheme;
			$data['alluser']= $this->User->findAllUsersBd();
			$data['allTheme']= $this->Theme->findAllThemeinbd();
			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('formulaireAddCat');
			$this->load->view('ADMIN/footer');
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}
	public function addTheme(){
		if (isset($_SESSION['ADMIN'])) {
			if(isset($_POST)){
				if (!empty($_POST['libelle']) AND !empty($_POST['id_cat']) AND !empty($_POST['id_user'])) {
					$data['id_user']=$_POST['id_user'];
					$data['id_cat']=$_POST['id_cat'];
					$data['id_cat']=$_POST['id_cat'];
					$data['libelle']=$_POST['libelle'];
					$data['niveau']=$_POST['niveau'];
					$data['date_creation']=date('Y-m-d H:i:s');
					$this->Theme->hydrate($data);
					$this->Theme->addTheme();
					redirect(site_url(array('Administration','liste_Theme')));
				}else{
					redirect(site_url(array('Administration','index')));
				}
			}else{redirect(site_url(array('Administration','liste_Theme')));}
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}
	public function addCategorie(){

			if (isset($_SESSION['ADMIN'])){
				if(isset($_POST)){
					if (isset($_POST['nom_cat'])AND isset($_FILES['photo_cat'])) {
						if ($_FILES['photo_cat']['size'] <= 100000000 ){
							if ($_FILES['photo_cat']['error']==0) {
								$extensions_autorisees = array('jpg', 'jpeg', 'png','JPEG','PNG','JPG');
								$infosfichier =pathinfo($_FILES['photo_cat']['name']);
								 $extension_upload = $infosfichier['extension'];
					            if (in_array($extension_upload, $extensions_autorisees))
					            {
					            	$config =$_FILES['photo_cat']['name'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i').$_SESSION['ADMIN']['id'];
					            	$ma_variable = str_replace('.', '_', $config);
									$ma_variable = str_replace(' ', '_', $config);
									$config = $ma_variable.'.'.$extension_upload;
									move_uploaded_file($_FILES['photo_cat']['tmp_name'],'assets/images/'.$config);
									$data['photo_cat']=$config;
					               $statut_img="ok";
					            }else{
					               $statut_img="non";
					            }
							}else{
								$erreur='non';
								$data['message_save']="L'image choisie  est endommagée  veuillez le remplacer svp !!";
							}
						}else{
							$data['message_save']="La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
							$data['message']='non';
						}
						if ($statut_img=="ok") {
							$data['nom_cat']=$_POST['nom_cat'];
							$data['id_user']=$_POST['id_user'];
							$data['niveau']=$_POST['niveau'];
							$data['date_creation']=date('Y-m-d H:i:s');
							$this->Categorie->hydrate($data);
						    $this->Categorie->addCategorie();
						    $_SESSION['message_save']="Categorie enregistré avec success !!";
				 		     $_SESSION['success']='ok';
				 		     redirect(site_url(array('Administration','listeCategorie')));
						}else{
							$data['message']='extension de l image non autorise';
							redirect(site_url(array('Administration','index')));
						}
						
			 		
					}
				}else{
					redirect(site_url(array('Administration','index')));
				}

			}else{
				session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
			}
	}
	public function formulaireaddCategorie(){
		if (isset($_SESSION['ADMIN'])){
			$allTheme=$this->Theme->findAllThememodel();
			$data['allnotiftheme']=$allTheme;
			$data['alluser']= $this->User->findAllUsersBd();
			$data['allTheme']= $this->Theme->findAllThemeinbd();
			$this->load->view('formulaireaddCategorie',$data);
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
 	}
	public function listeCategorie(){
		if(isset($_SESSION['ADMIN'])){
			$Tcate = $this->Categorie->findAllCategorieBd();
			$allUser=$this->User->findAdmin();
			$allUser1=$this->User->findmoderateur();
			$trouve = 'non';
			for($i=0; $i<$Tcate['total']; $i++){
				$alltheme[$i]= $this->Theme->findThemeinbd($Tcate[$i]['id']);
			 	for ($j=0; $j<$allUser['total']; $j++){
			 		if($Tcate[$i]['id_user']==$allUser[$j]['id']){
			 			$Tcate[$i]['createur']= $allUser[$j]['nom'].' '.$allUser[$j]['prenom'];
			 			//$trouve = 'ok';
			 			break;
			 		}
			 	}
			}
			if ($trouve == 'non') {
				// $erreur = 'non';
				for($i=0; $i<$Tcate['total']; $i++){
				 	for ($j=0; $j<$allUser1['total']; $j++){
				 		if($Tcate[$i]['id_user']==$allUser1[$j]['id']){
				 			$Tcate[$i]['createur']= $allUser1[$j]['nom'].' '.$allUser1[$j]['prenom'];
				 			// $erreur = 'ok';
				 			break;
				 		}
				 	}
				}
			}
			for ($i=0; $i <$Tcate['total'] ; $i++) { 
				$alltheme[$i]=$this->Theme->findThemeinbd($Tcate[$i]['id']);
			}
			$data['alltheme']=$alltheme ;
			$data['nom']=$Tcate;
			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('listeCategorie',$data);
			$this->load->view('ADMIN/footer');
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}

	}
	
	// fonction pour nommer un moderateur
	public function AddModerateur() {
		if (isset($_SESSION['ADMIN'])) {
            $this->User->UpdateUser($_POST['cible']);
            $cible =  $this->Abonne->findabonneInfos($_POST['cible']);
            $data['id'] = $cible['id'];
            $data['id_user'] = $cible['id_user'];
            $data['email'] = $cible['email'];
            $data['password'] = $cible['password'];
            $this->Moderateur->hydrate($data);
            $this->Moderateur->AddModerator();
            redirect(site_url(array('Administration','CandidatureForMod')));
        }else{
            session_destroy();
            redirect(site_url(array('Administration','formulaireConnexion')));
        }
	}

	// fonction pour afficher la liste des utilisateurs poouvant etre nommer comme moderateur
	public function CandidatureForMod() {
		 if (isset($_SESSION['ADMIN'])) {
			$listabonne['forum'] = $this->User->findAllUserBd();
			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/candidature',$listabonne);
			$this->load->view('ADMIN/footer');
		}else{
	   	 session_destroy();
		 redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}

	// fonction pour afficher la liste des moderateur pour leur retirer leur titre
	public function ForDemodulation() {
		if (isset($_SESSION['ADMIN'])) {
			$Moderateur = $this->Moderateur->findAllMod();
			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/deletemod',$Moderateur);
			$this->load->view('ADMIN/footer');
		}else{
	   	 session_destroy();
		 redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}

	public function theme_categorie(){
		if(isset($_SESSION['ADMIN'])){
			if (isset($_POST)) {
				
				$theme=$this->Theme->findAllThemeinbd();
				$allUser=$this->User->findAdmin();
				$allUser1=$this->User->findmoderateur();
				$trouve='non';
					$j=0;
					$s=0;
				for ($i=0; $i<$theme['total']; $i++){
					if($theme[$i]['id_cat']==$_POST['id']){
						$themeok[$s]=$theme[$i];
						$s++;

					}
				} 

				for($i=0; $i<$s; $i++ ){
					for($k=0; $k<$allUser['total']; $k++){
						if($themeok[$i]['id_user']==$allUser[$k]['id']){
							$themeok[$i]['nom']=$allUser[$k]['nom'].' '.$allUser[$k]['prenom'];
							//$trouve='ok';
							break;
						}
					}

				}

				if ($trouve =='non') {
					for($i=0; $i<$s; $i++ ){
						for($k=0; $k<$allUser1['total']; $k++){
							if($themeok[$i]['id_user']==$allUser1[$k]['id']){
								$themeok[$i]['nom']=$allUser1[$k]['nom'].' '.$allUser1[$k]['prenom'];
								$trouve='ok';
								break;
							}
						}

					}
				}
				
				$themeok['categorie']=$_POST['nom_cat'];
				$themeok['taille']=$s;
				$data['themes']=$themeok;
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('categorie_theme',$data);
				$this->load->view('ADMIN/footer');
	        }else{
	        	redirect(site_url(array('Administration','listeCategorie')));
	        }
        }else{
	   	 session_destroy();
		 redirect(site_url(array('Administration','formulaireConnexion')));
		}
  			
    }

	// fonction pour supprimer un moderateur
	public function deletemod() {
		 if (isset($_SESSION['ADMIN'])) {
			$this->User->chuterUser($_POST['cible']);
			$cible =  $this->Abonne->findabonneInfos($_POST['cible']);
			$this->Moderateur->DelModerator($cible);
			redirect(site_url(array('Administration','ForDemodulation')));
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}


	public function liste_Theme(){
		if (isset($_SESSION['ADMIN'])) {
			$theme=$this->Theme->findAllThemeinbd();
			for ($i=0; $i <$theme['total'] ; $i++) { 
				$iduser[$i]=$theme[$i]['id_user'];
				$idcategorie[$i]=$theme[$i]['id_cat'];
			}
			for ($i=0; $i <$theme['total'] ; $i++) { 
				$user= $this->User->finduserInfos($iduser[$i]);
				$theme[$i]['nom']= $user['nom'].' '.$user['prenom'];
			}

			for ($i=0; $i <$theme['total'] ; $i++) { 
				$categorie= $this->Categorie->findcategorieInfos($idcategorie[$i]);
				$theme[$i]['categorie']=$categorie['nom_cat'];
				$comment[$i]=$this->Commentaire->findCommentaire($theme[$i]['id']);
			}
			$data['themes']=$theme;
			$data['comment']=$comment;
			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('listeTheme',$data);
			$this->load->view('ADMIN/footer');
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}

	public function supprimerTheme(){
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST) AND !empty($_POST)) {
				 $this->Theme->deleteTheme($_POST['id']);
				 redirect(site_url(array('Administration','liste_Theme')));
			}else{
				redirect(site_url(array('Administration','liste_Theme')));
			}
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}


	public function formulairemodifCategorie(){
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST) AND !empty($_POST)) {
				$ids= $_POST['id'];
				$categorieinfos= $this->Categorie->findcategorieInfos($ids);
				$data['infocategorie']=$categorieinfos;
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('modifierCategorie',$data);
				$this->load->view('ADMIN/footer');
			}else{
				redirect(site_url(array('Administration','listeCategorie')));
			}
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}

	// fonction pour afficher la liste des utilisateurs en vue de les bloquer
	public function listBloquerUser() {
		 if (isset($_SESSION['ADMIN'])) {
			$listabonne['forum'] = $this->User->findAllUserBd();
			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/bloqueruser',$listabonne);
			$this->load->view('ADMIN/footer');
		}else{
	   	 session_destroy();
		 redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}

	public function modifierCategorie(){
		if (isset($_SESSION['ADMIN'])){
				if(isset($_POST)){
					if (!empty($_POST['newnom_cat'])AND !empty($_FILES['newphoto_cat'])) {
						if ($_FILES['newphoto_cat']['size'] <= 100000000 ){
							if ($_FILES['newphoto_cat']['error']==0) {
								$extensions_autorisees = array('jpg', 'jpeg', 'png','JPEG','PNG','JPG');
								$infosfichier =pathinfo($_FILES['newphoto_cat']['name']);
								 $extension_upload = $infosfichier['extension'];
					            if (in_array($extension_upload, $extensions_autorisees))
					            {
					            	$config =$_FILES['newphoto_cat']['name'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i');
					            	$ma_variable = str_replace('.', '_', $config);
									$ma_variable = str_replace(' ', '_', $config);
									$config = $ma_variable.'.'.$extension_upload;
									move_uploaded_file($_FILES['newphoto_cat']['tmp_name'],'assets/images/'.$config);
									$data['photo_cat']=$config;
					               $statut_img="ok";
					            }else{
					               $statut_img="non";
					            }
							}else{
								$erreur='non';
								$data['message_save']="L'image choisie  est endommagée  veuillez le remplacer svp !!";
							}
						}else{
							$data['message_save']="La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
							$data['message']='non';
						}
						if ($statut_img=="ok") {
							$data['nom_cat']=$_POST['newnom_cat'];
							//$data['id_user']=$_POST['id_user'];
							//$data['niveau']=$_POST['niveau'];
							//$data['date_creation']=date('Y-m-d H:i:s');
							$this->Categorie->hydrate($data);
						    $this->Categorie->UpdateCategorie($_POST['id_cat'],$_POST['newnom_cat'],$data['photo_cat'],$_POST['date_modification']);
						    $_SESSION['message_save']="Categorie mofifier avec success !!";
				 		     $_SESSION['success']='ok';
				 		     redirect(site_url(array('Administration','listeCategorie')));
						}else{
							$data['message']='extension de l image non autorise';
							redirect(site_url(array('Administration','index')));
						}
						
			 		
					}
				}else{
					redirect(site_url(array('Administration','index')));
				}

			}else{
				session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
			}
	}


	public function formcommenterTheme(){
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST) AND !empty($_POST)) {
				print_r($_POST);
				$data['post']=$_POST;
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('commenterTheme',$data);
				$this->load->view('ADMIN/footer');
			}else{
				redirect('Administration','liste_Theme');
			}
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}


	public function addCommentaire(){
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST) AND !empty($_POST)) {
				$data['contenu']=$_POST['comment'];
				$data['id_theme']=$_POST['id_theme'];
				$data['date_creation']=$_POST['date_creation'];
				$data['statut']=$_POST['niveau'];
				$data['id_user']=$_POST['id_user'];
				$this->Commentaire->hydrate($data);
				$this->Commentaire->addCommentaire();
				$this->Theme->empechersuppression($_POST['id_theme']);
				redirect('Administration','liste_Theme');
				print_r($data);
			}else{
				redirect('Administration','formcommenterTheme');
			}
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}

	// fonction pour bloquer un utilisateur en BD
	public function BloquerUser() {
		  if (isset($_SESSION['ADMIN'])) {
			$this->User->blocUser($_POST['cible']);
			redirect(site_url(array('Administration','listBloquerUser')));
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}


	// fonction pour afficher la liste des utilisateurs bloque
	public function listDebloquerUser() {
		 if (isset($_SESSION['ADMIN'])) {
			$listabonne['forum'] = $this->User->findBlockUser();
			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/debloqueruser',$listabonne);
			$this->load->view('ADMIN/footer');
		}else{
	   	 session_destroy();
		 redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}

	// fonction pour debloquer un utilisateur en BD
	public function DebloquerUser() {
		  if (isset($_SESSION['ADMIN'])) {
			$this->User->deblocUser($_POST['cible']);
			redirect(site_url(array('Administration','listDebloquerUser')));
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}

	public function notif_theme(){
		
		if (isset($_SESSION['ADMIN'])) {
			
			$allTheme=$this->Theme->findAllThememodel();
			$allcategorie=$this->Categorie->findAllCategoriemodel();
			$data['allnotiftheme']=$allTheme;
			$data['allnotifcategorie']=$allcategorie;
			$this->load->view('ADMIN/index',$data);
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/notif_theme', $data);
			$this->load->view('ADMIN/footer');
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}

	public function notif_categorie(){
		
		if (isset($_SESSION['ADMIN'])) {
			
			$allcategorie=$this->Categorie->findAllCategoriemodel();
			$allTheme=$this->Theme->findAllThememodel();
			$data['allnotifcategorie']=$allcategorie;
			$data['allnotiftheme']=$allTheme;
			$this->load->view('ADMIN/index',$data);
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/notif_categorie', $data);
			$this->load->view('ADMIN/footer');
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}

	public function notif_signalement(){
		
		if (isset($_SESSION['ADMIN'])) {
			
			$allnotifsignalement=$this->Signalement->findAllSignalementmodel();
			$data['allnotifsignalement']=$allnotifsignalement;
			// synthaxe aui permet de recuperer la valeur d'un attribu d'une clé primaire
			for ($i=0; $i <$data['allnotifsignalement']['total'] ; $i++) { 
				$data['allLibelleTheme'][$i]=$this->Theme->findthemeInfos($data['allnotifsignalement'][$i]['id_theme']);
			}

			$this->load->view('ADMIN/index',$data);
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/notif_signalement', $data);
			$this->load->view('ADMIN/footer');
			print_r($allnotifsignalement);
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}

	public function notif_signalement_commentaire(){
		
		if (isset($_SESSION['ADMIN'])) {
			
			$allnotifsignalement=$this->Signalement->findAllSignalementcommentairemodel();
			$data['allnotifsignalement']=$allnotifsignalement;
			for ($i=0; $i <$allnotifsignalement['total'] ; $i++) { 
				$allContenuCommentaire[$i]=$this->Commentaire->findCommentaireInfos($allnotifsignalement[$i]['id_commentaire']);
			}
			$this->load->view('ADMIN/index',$data);
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/notif_signalement_commentaire', $data);
			$this->load->view('ADMIN/footer');
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}


	public function supprimerCategorie(){
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST)) {
				$this->Categorie->deleteCategorie($_POST['id_cat']);
				redirect(site_url(array('Administration','listeCategorie')));

			}
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}


	public function formodifiertheme(){
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST)) {
				$Tcate = $this->Categorie->findAllCategorieBd();
				$data['nom']=$Tcate;
				$data['id_theme']=$_POST['id_theme'];
				$data['libelle']=$_POST['lib'];
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('formodifiertheme', $data);
				$this->load->view('ADMIN/footer');
				}else{
				redirect(site_url(array('Administration','liste_Theme')));
			}
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}


	public function modifierTheme (){
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST)) {
				$data['libelle']=$_POST['newlibelle'];
				$data['date_modification']=$_POST['datemodif'];
				$data['id_cat']=$_POST['newid_cat'];
				$this->Theme->hydrate($data);
				$this->Theme->UpdateTheme($_POST['id_theme']);
				redirect(site_url(array('Administration','liste_Theme')));
			}else{
				redirect(site_url(array('Administration','liste_Theme')));
			}
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}


	public function commentaire_theme(){

		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST)) {
				$allcomment=$this->Commentaire->findCommentaire($_POST['id_theme']);
				for ($i=0; $i <$allcomment['total'] ; $i++) { 
					$user=$this->User->finduserInfos($allcomment[$i]['id_user']);
					$allcomment[$i]['auteur']= $user['nom'].' '.$user['prenom'];

				}
				$allcomment['categorie']=$_POST['categorie'];
				$allcomment['theme']=$_POST['theme'];
				$data['comment']=$allcomment;
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('commentaire_theme', $data);
				$this->load->view('ADMIN/footer');
			}else{
					redirect(site_url(array('Administration','liste_Theme')));
				}
		}else{
				session_destroy();
				redirect(site_url(array('Administration','formulaireConnexion')));
			}
	}


	public function supprimerCommentaire(){
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST)) {
				$this->Commentaire->deleteCommentaire($_POST['id_commentaire']);
				redirect(site_url(array('Administration','commentaire_theme')));
			}
		}else{
				session_destroy();
				redirect(site_url(array('Administration','formulaireConnexion')));
			}
	}


	public function voirprofil(){
		if (isset($_SESSION['ADMIN'])) {
			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/profil');
			$this->load->view('ADMIN/footer');
		}else{
				session_destroy();
				redirect(site_url(array('Administration','formulaireConnexion')));
			}
	}

	public function formmodifierprofil(){
		if (isset($_SESSION['ADMIN'])) {
			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/modifierprofil');
			$this->load->view('ADMIN/footer');	
		}else{
				session_destroy();
				redirect(site_url(array('Administration','formulaireConnexion')));
			}
	}

	public function modifierprofil(){
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST) AND !empty($_POST)) {

				if ($_POST['newnom']!=$_SESSION['ADMIN']['nom']) {
					$data['nom']=$_POST['newnom'];
				}else{
					$data['nom']=$_SESSION['ADMIN']['nom'];
				}
				if ($_POST['newprenom']!=$_SESSION['ADMIN']['prenom']) {
					$data['prenom']=$_POST['newprenom'];
				}else{
					$data['prenom']=$_SESSION['ADMIN']['prenom'];
				}
				if ($_POST['newemail']!=$_SESSION['ADMIN']['email']) {
					$data['email']=$_POST['newemail'];
				}else{
					$data['email']=$_SESSION['ADMIN']['email'];
				}
				if ($_FILES['newprofil']['name']!=$_SESSION['ADMIN']['profil']) {
					if ($_FILES['newprofil']['size'] <= 100000000 ){
						if ($_FILES['newprofil']['error']==0) {
							$extensions_autorisees = array('jpg', 'jpeg', 'png','JPEG','PNG','JPG');
							$infosfichier =pathinfo($_FILES['newprofil']['name']);
							 $extension_upload = $infosfichier['extension'];
				            if (in_array($extension_upload, $extensions_autorisees))
				            {
				            	$config =$infosfichier['filename'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i');
				            	$ma_variable = str_replace('.', '_', $config);
								$ma_variable = str_replace(' ', '_', $config);
								$config = $ma_variable.'.'.$extension_upload;
								move_uploaded_file($_FILES['newprofil']['tmp_name'],'assets/images/user_profil/'.$config);
								$data['profil']=$config;
				               $statut_img="ok";
				            }else{
				               $statut_img="non";
				            }
				            if ($statut_img=="ok") {
				            	$data['id']=$_SESSION['ADMIN']['id'];
				            	$data['id_user']=$_SESSION['ADMIN']['id_user'];
				            	$this->Admin->hydrate($data);
				            	$this->Admin->UpdateAdmin();
				            	$this->User->hydrate($data);
				            	$this->User->UpdateUserinfo();
				          		$this->load->view('ADMIN/index');
								$this->load->view('template_al/navigation');
								$this->load->view('ADMIN/profil');
								$this->load->view('ADMIN/footer');
								$_SESSION['message_save']='modification effective';
								$_SESSION['ADMIN']['nom']=$data['nom'];
								$_SESSION['ADMIN']['prenom']=$data['prenom'];
								$_SESSION['ADMIN']['email']=$data['email'];
								$_SESSION['ADMIN']['profil']=$data['profil'];
				            }else{
				            	redirect(site_url(array('Administration','formmodifierprofil')));
				            	$_SESSION['message_error']='!!echec de modification';
				            }
						}else{
							$erreur='non';
							$data['message_save']="L'image choisie  est endommagée  veuillez le remplacer svp !!";
						}
					}else{
						$data['message_save']="La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
						$data['message']='non';
						print_r($_FILES);
					}
				}else{
					print_r($_FILES);
				}
				
			}else{
				redirect(site_url(array('Administration','voirprofil')));
			}
		}else{
				session_destroy();
				redirect(site_url(array('Administration','formulaireConnexion')));
			}
	}



}
