<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	class Moderateur extends CI_Controller {

		public function index(){
			if (isset($_SESSION['Moderateur'])) {
			$allcategorie=$this->Categorie->findAllCategoriemodel();
			$data['allnotifcategorie']=$allcategorie;

			$allTheme=$this->Theme->findAllThememodel();
			$data['allnotiftheme']=$allTheme;
			$data['alluser']= $this->User->findAllUsersBd();
			$data['allTheme']= $this->Theme->findAllThemeinbd();
			$data['allContenuCommentaire']= $this->Commentaire->findallcommentairebd();
				$data['allnotiftheme']=$allTheme;
				$this->load->view('ADMIN/index',$data);
				$this->load->view('template_al/navigation');
				$this->load->view('ADMIN/home');
				$this->load->view('ADMIN/footer');
			} else{
				session_destroy();
				redirect(site_url(array('Welcome','inscription')));
			}
		}


		public function formulaireConnexion(){
		
			if (isset($_SESSION['Moderateur'])) {
				if (isset($_SESSION['Moderateur'])) {
					redirect(site_url(array('Administration','index')));
				}else{
					session_destroy();
					redirect(site_url(array('Administration','formulaireConnexion')));
				}
			}else{
				$this->load->view('gestion_admin/formulaire_connexion');
			}
		}

	
		// gestion connexion des moderateurs
		public function traitementConnexion(){
			if ( isset($_SESSION['mail'] ) && isset( $_SESSION['pswd'] ) ){
				$Moderateur = $this->Moderateur->findAllModerateur();
				for ( $i=0; $i < $Moderateur['total']; $i++ ) { 
					if ( $Moderateur[$i]['email'] == $_SESSION['mail'] && $Moderateur[$i]['password'] == $_SESSION['pswd'] ) {
							$cible = $Moderateur[$i]['id_user'];
							$cordonnees = $this->User->finduserInfos($cible);
							$_SESSION['Moderateur']['id_user'] = $Moderateur[$i]['id_user'];
							$_SESSION['Moderateur']['nom'] = $cordonnees['nom'];
							$_SESSION['Moderateur']['prenom'] = $cordonnees['prenom'] ;
							$_SESSION['Moderateur']['profil'] = $cordonnees['profil'];
							$_SESSION['Moderateur']['notif'] = $cordonnees['notif'];

							$_SESSION['Moderateur']['email'] = $_SESSION['mail'];

							$_SESSION['Moderateur']['id'] = $Moderateur[$i]['id'];

					}
				}
				
				if (isset($_SESSION['Moderateur'])) {
					redirect(site_url(array('Moderateur','index')));
				}else{
					// redirect(site_url(array('Abonne','formulaireconnexion')));
					$_SESSION['ERROR'] = 'Les paramtres recus ne correspondent a auccun compte.<br> <b> Merci de Reessayer :-) </b>';
					// print_r($_SESSION['ERROR']);
				}
			}
			else{
				session_destroy();
				redirect(site_url(array('Abonne','formulaireconnexion')));
			}
		}


		public function formulaireAddTheme(){
			if(isset($_SESSION['Moderateur'])){
				$Tcate = $this->Categorie->findAllCategorieBd();
				$data['nom']=$Tcate;
				 $this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('formulaireAddTheme',$data);
				$this->load->view('ADMIN/footer');
			}else{
				session_destroy();
				redirect(site_url(array('Moderateur','formulaireConnexion')));
			}
		}


		public function liste_Theme(){
			if (isset($_SESSION['Moderateur'])) {
				$theme=$this->Theme->findAllThemeinbd();
				for ($i=0; $i <$theme['total'] ; $i++) { 
					$iduser[$i]=$theme[$i]['id_user'];
					$idcategorie[$i]=$theme[$i]['id_cat'];
				}
				for ($i=0; $i <$theme['total'] ; $i++) { 
					$user= $this->User->finduserInfos($iduser[$i]);
					$theme[$i]['nom']= $user['nom'].' '.$user['prenom'];
				}

				for ($i=0; $i <$theme['total'] ; $i++) { 
					$categorie= $this->Categorie->findcategorieInfos($idcategorie[$i]);
					$theme[$i]['categorie']=$categorie['nom_cat'];
					$comment[$i]=$this->Commentaire->findCommentaire($theme[$i]['id']);
				}
				$data['themes']=$theme;
				$data['comment']=$comment;
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('listeTheme',$data);
				$this->load->view('ADMIN/footer');
			}else{
				session_destroy();
				redirect(site_url(array('Abonne','formulaireConnexion')));
			}
	    }
	    



	    public function addTheme(){
			if (isset($_SESSION['Moderateur'])) {
				if(isset($_POST)){
					if (!empty($_POST['libelle']) AND !empty($_POST['id_cat']) AND !empty($_POST['id_user'])) {
						$data['id_user']=$_POST['id_user'];
						$data['id_cat']=$_POST['id_cat'];
						$data['id_cat']=$_POST['id_cat'];
						$data['libelle']=$_POST['libelle'];
						$data['niveau']=$_POST['niveau'];
						$data['date_creation']=date('Y-m-d H:i:s');
						$this->Theme->hydrate($data);
						$this->Theme->addTheme();
						redirect(site_url(array('Moderateur','liste_Theme')));
					}else{
						redirect(site_url(array('Moderateur','index')));
					}
				}
			}else{
				session_destroy();
				redirect(site_url(array('Abonne','formulaireConnexion')));
			}
	   }



	   public function commentaire_theme(){

				if (isset($_SESSION['Moderateur'])) {
					if (isset($_POST)) {
						$allcomment=$this->Commentaire->findCommentaire($_POST['id_theme']);
						for ($i=0; $i <$allcomment['total'] ; $i++) { 
							$user=$this->User->finduserInfos($allcomment[$i]['id_user']);
							$allcomment[$i]['auteur']= $user['nom'].' '.$user['prenom'];

						}
						$allcomment['categorie']=$_POST['categorie'];
						$allcomment['theme']=$_POST['theme'];
						$data['comment']=$allcomment;
						$this->load->view('ADMIN/index');
						$this->load->view('template_al/navigation');
						$this->load->view('commentaire_theme', $data);
						$this->load->view('ADMIN/footer');
					}else{
							redirect(site_url(array('Moderateur','liste_Theme')));
						}
				}else{
						session_destroy();
						redirect(site_url(array('Moderateur','formulaireConnexion')));
					}
	   }


		// fonction pour deconnecter un Moderateur
		public function deconnexion(){
			if (isset($_SESSION['Moderateur'])) {
				session_destroy();
			}
			redirect(site_url(array('Welcome','index')));
		}

		public function formAddCat(){
			if(isset($_SESSION['Moderateur'])){
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('formulaireAddCat');
				$this->load->view('ADMIN/footer');
			}else{
				session_destroy();
				redirect(site_url(array('Abonne','formulaireConnexion')));
			}
		}

		public function addCat(){

				if (isset($_SESSION['Moderateur'])){
					if(isset($_POST)){
						if (isset($_POST['nom_cat'])AND isset($_FILES['photo_cat'])) {
							if ($_FILES['photo_cat']['size'] <= 100000000 ){
								if ($_FILES['photo_cat']['error']==0) {
									$extensions_autorisees = array('jpg', 'jpeg', 'png','JPEG','PNG','JPG');
									$infosfichier =pathinfo($_FILES['photo_cat']['name']);
									 $extension_upload = $infosfichier['extension'];
						            if (in_array($extension_upload, $extensions_autorisees))
						            {
						            	$config =$infosfichier['filename'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i').$_SESSION['Moderateur']['id'];
						            	$ma_variable = str_replace('.', '_', $config);
										$ma_variable = str_replace(' ', '_', $config);
										$config = $ma_variable.'.'.$extension_upload;
										move_uploaded_file($_FILES['photo_cat']['tmp_name'],'assets/images/'.$config);
										$data['photo_cat']=$config;
						               $statut_img="ok";
						            }else{
						               $statut_img="non";
						            }
								}else{
									$erreur='non';
									$data['message_save']="L'image choisie  est endommagée  veuillez le remplacer svp !!";
								}
							}else{
								$data['message_save']="La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
								$data['message']='non';
							}
							if ($statut_img=="ok") {
								$data['nom_cat']=$_POST['nom_cat'];
								$data['id_user']=$_POST['id_user'];
								$data['niveau']=$_POST['niveau'];
								$data['date_creation']=date('Y-m-d H:i:s');
								$this->Categorie->hydrate($data);
							    $this->Categorie->addCategorie();
							    $_SESSION['message_save']="Categorie enregistré avec success !!";
					 		     $_SESSION['success']='ok';
					 		     redirect(site_url(array('Moderateur','index')));
							}else{
								$data['message']='extension de l image non autorise';
								redirect(site_url(array('Moderateur','index')));
							}
							
				 		
						}
					}else{
						redirect(site_url(array('Moderateur','index')));
					}

				}else{
					session_destroy();
				redirect(site_url(array('Abonne','formulaireConnexion')));
				}
		}


		public function Categorielist(){
			if(isset($_SESSION['Moderateur'])){
				$Tcate = $this->Categorie->findAllCategorieBd();
				$allUser1=$this->User->findAdmin();
				$allUser=$this->User->findmoderateur();
				$trouve = 'non';
				for($i=0; $i<$Tcate['total']; $i++){
				 	for ($j=0; $j<$allUser['total']; $j++){
				 		if($Tcate[$i]['id_user']==$allUser[$j]['id']){
				 			$Tcate[$i]['createur']= $allUser[$j]['nom'].' '.$allUser[$j]['prenom'];
				 			// $trouve = 'ok';
				 			break;
				 		}
				 	}
				}
				if ($trouve == 'non') {
					// $erreur = 'non';
					for($i=0; $i<$Tcate['total']; $i++){
					 	for ($j=0; $j<$allUser1['total']; $j++){
					 		if($Tcate[$i]['id_user']==$allUser1[$j]['id']){
					 			$Tcate[$i]['createur']= $allUser1[$j]['nom'].' '.$allUser1[$j]['prenom'];
					 			// $erreur = 'ok';
					 			$trouve = 'ok';
					 			break;
					 		}
					 	}
					}
				}
				for ($i=0; $i <$Tcate['total'] ; $i++) { 
					$alltheme[$i]=$this->Theme->findThemeinbd($Tcate[$i]['id']);
				}
				$data['alltheme']=$alltheme ;
				$data['nom']=$Tcate;
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('listeCategorie',$data);
				$this->load->view('ADMIN/footer');
			}else{
				session_destroy();
				redirect(site_url(array('Abonne','formulaireConnexion')));
			}

		}

		public function themeCategorie(){
			if(isset($_SESSION['Moderateur'])){
						if (isset($_POST)) {
							
							$theme=$this->Theme->findAllThemeinbd();
							$allUser1=$this->User->findAdmin();
							$allUser=$this->User->findmoderateur();
							$trouve='non';
								$j=0;
								$s=0;
							for ($i=0; $i<$theme['total']; $i++){
								if($theme[$i]['id_cat']==$_POST['id']){
									$themeok[$s]=$theme[$i];
									$s++;

								}
							} 

							for($i=0; $i<$s; $i++ ){
								for($k=0; $k<$allUser['total']; $k++){
									if($themeok[$i]['id_user']==$allUser[$k]['id']){
										$themeok[$i]['nom']=$allUser[$k]['nom'].' '.$allUser[$k]['prenom'];
										break;
									}
								}

							}

							if ($trouve =='non') {
								for($i=0; $i<$s; $i++ ){
									for($k=0; $k<$allUser1['total']; $k++){
										if($themeok[$i]['id_user']==$allUser1[$k]['id']){
											$themeok[$i]['nom']=$allUser1[$k]['nom'].' '.$allUser1[$k]['prenom'];
											$trouve='ok';
											break;
										}
									}

								}
							}
							
							$themeok['categorie']=$_POST['nom_cat'];
							$themeok['taille']=$s;
							$data['themes']=$themeok;
							$this->load->view('ADMIN/index');
							$this->load->view('template_al/navigation');
							$this->load->view('categorie_theme',$data);
							$this->load->view('ADMIN/footer');
				        }
			           }
			  			
		}

		public function FormModifCategorie(){
			if (isset($_SESSION['Moderateur'])) {
				if (isset($_POST) AND !empty($_POST)) {
					$ids= $_POST['id'];
					$categorieinfos= $this->Categorie->findcategorieInfos($ids);
					$data['infocategorie']=$categorieinfos;
					$this->load->view('ADMIN/index');
					$this->load->view('template_al/navigation');
					$this->load->view('modifierCategorie',$data);
					$this->load->view('ADMIN/footer');
					}
			}
		}

		public function modifCategorie(){
			if (isset($_SESSION['Moderateur'])){
					if(isset($_POST)){
						if (!empty($_POST['newnom_cat'])AND !empty($_FILES['newphoto_cat'])) {
							if ($_FILES['newphoto_cat']['size'] <= 100000000 ){
								if ($_FILES['newphoto_cat']['error']==0) {
									$extensions_autorisees = array('jpg', 'jpeg', 'png','JPEG','PNG','JPG');
									$infosfichier =pathinfo($_FILES['newphoto_cat']['name']);
									 $extension_upload = $infosfichier['extension'];
						            if (in_array($extension_upload, $extensions_autorisees))
						            {
						            	$config =$infosfichier['filename'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i');
						            	$ma_variable = str_replace('.', '_', $config);
										$ma_variable = str_replace(' ', '_', $config);
										$config = $ma_variable.'.'.$extension_upload;
										move_uploaded_file($_FILES['newphoto_cat']['tmp_name'],'assets/images/'.$config);
										$data['photo_cat']=$config;
						               $statut_img="ok";
						            }else{
						               $statut_img="non";
						            }
								}else{
									$erreur='non';
									$data['message_save']="L'image choisie  est endommagée  veuillez le remplacer svp !!";
								}
							}else{
								$data['message_save']="La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
								$data['message']='non';
							}
							if ($statut_img=="ok") {
								$data['nom_cat']=$_POST['newnom_cat'];
								//$data['id_user']=$_POST['id_user'];
								//$data['niveau']=$_POST['niveau'];
								//$data['date_creation']=date('Y-m-d H:i:s');
								$this->Categorie->hydrate($data);
							    $this->Categorie->UpdateCategorie($_POST['id_cat'],$_POST['newnom_cat'],$data['photo_cat'],$_POST['date_modification']);
							    $_SESSION['message_save']="Categorie enregistré avec success !!";
					 		     $_SESSION['success']='ok';
					 		     redirect(site_url(array('Moderateur','Categorielist')));
							}else{
								$data['message']='extension de l image non autorise';
								redirect(site_url(array('Moderateur','index')));
							}
							
				 		
						}
					}else{
						redirect(site_url(array('Moderateur','index')));
					}

				}else{
					session_destroy();
				redirect(site_url(array('Abonne','formulaireConnexion')));
				}
		}


		public function supprCategorie(){
			if (isset($_SESSION['Moderateur'])) {
				if (isset($_POST)) {
					$this->Categorie->deleteCategorie($_POST['id_cat']);
					redirect(site_url(array('Moderateur','Categorielist')));

				}
			}else{
				session_destroy();
				redirect(site_url(array('Abonne','formulaireConnexion')));
			}
		}

		public function supprimerTheme(){
			if (isset($_SESSION['Moderateur'])) {
				if (isset($_POST) AND !empty($_POST)) {
					 $this->Theme->deleteTheme($_POST['id']);
					 redirect(site_url(array('Moderateur','liste_Theme')));
				}
			}else{
					session_destroy();
					redirect(site_url(array('Abonne','formulaireConnexion')));
				}
	    }


	    public function formcommenterTheme(){
			if (isset($_SESSION['Moderateur'])) {
				if (isset($_POST) AND !empty($_POST)) {
					print_r($_POST);
					$data['post']=$_POST;
					$this->load->view('ADMIN/index');
					$this->load->view('template_al/navigation');
					$this->load->view('commenterTheme',$data);
					$this->load->view('ADMIN/footer');
				}else{
					redirect('Moderateur','liste_Theme');
				}
			}else{
				session_destroy();
				redirect(site_url(array('Abonne','formulaireConnexion')));
			}
		}



		public function formodifiertheme(){
			if (isset($_SESSION['Moderateur'])) {
				if (isset($_POST)) {
					$Tcate = $this->Categorie->findAllCategorieBd();
					$data['nom']=$Tcate;
					$data['id_theme']=$_POST['id_theme'];
					$data['libelle']=$_POST['lib'];
					$this->load->view('ADMIN/index');
					$this->load->view('template_al/navigation');
					$this->load->view('formodifiertheme', $data);
					$this->load->view('ADMIN/footer');
					}else{
					redirect(site_url(array('Moderateur','liste_Theme')));
				}
			}else{
				session_destroy();
				redirect(site_url(array('Abonne','formulaireConnexion')));
			}
	    }


	    public function addCommentaire(){
			if (isset($_SESSION['Moderateur'])) {
				if (isset($_POST) AND !empty($_POST)) {
					$data['contenu']=$_POST['comment'];
					$data['id_theme']=$_POST['id_theme'];
					$data['date_creation']=$_POST['date_creation'];
					$data['statut']=$_POST['niveau'];
					$data['id_user']=$_POST['id_user'];
					$this->Commentaire->hydrate($data);
					$this->Commentaire->addCommentaire();
					redirect('Moderateur','liste_Theme');
				}else{
					redirect('Moderateur','formcommenterTheme');
				}
			}else{
				session_destroy();
				redirect(site_url(array('Abonne','formulaireConnexion')));
			}
	    }


	    public function voirprofil(){
			if (isset($_SESSION['Moderateur'])) {
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('ADMIN/profil');
				$this->load->view('ADMIN/footer');
			}else{
					session_destroy();
					redirect(site_url(array('Moderateur','formulaireConnexion')));
				}
		}




		public function formmodifierprofil(){
			if (isset($_SESSION['Moderateur'])) {
				$this->load->view('ADMIN/index');
				$this->load->view('template_al/navigation');
				$this->load->view('ADMIN/modifierprofil');
				$this->load->view('ADMIN/footer');	
			}else{
					session_destroy();
					redirect(site_url(array('Administration','formulaireConnexion')));
				}
		}


		public function modifierprofil(){
		if (isset($_SESSION['Moderateur'])) {
			if (isset($_POST) AND !empty($_POST)) {

				if ($_POST['newnom']!=$_SESSION['Moderateur']['nom']) {
					$data['nom']=$_POST['newnom'];
				}else{
					$data['nom']=$_SESSION['Moderateur']['nom'];
				}
				if ($_POST['newprenom']!=$_SESSION['Moderateur']['prenom']) {
					$data['prenom']=$_POST['newprenom'];
				}else{
					$data['prenom']=$_SESSION['Moderateur']['prenom'];
				}
				if ($_POST['newemail']!=$_SESSION['Moderateur']['email']) {
					$data['email']=$_POST['newemail'];
				}else{
					$data['email']=$_SESSION['Moderateur']['email'];
				}
				if ($_FILES['newprofil']['name']!=$_SESSION['Moderateur']['profil']) {
					if ($_FILES['newprofil']['size'] <= 100000000 ){
						if ($_FILES['newprofil']['error']==0) {
							$extensions_autorisees = array('jpg', 'jpeg', 'png','JPEG','PNG','JPG');
							$infosfichier =pathinfo($_FILES['newprofil']['name']);
							 $extension_upload = $infosfichier['extension'];
				            if (in_array($extension_upload, $extensions_autorisees))
				            {
				            	$config =$infosfichier['filename'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i');
				            	$ma_variable = str_replace('.', '_', $config);
								$ma_variable = str_replace(' ', '_', $config);
								$config = $ma_variable.'.'.$extension_upload;
								move_uploaded_file($_FILES['newprofil']['tmp_name'],'assets/images/user_profil/'.$config);
								$data['profil']=$config;
				               $statut_img="ok";
				            }else{
				               $statut_img="non";
				            }
				            if ($statut_img=="ok") {
				            	$data['id']=$_SESSION['Moderateur']['id'];
				            	$data['id_user']=$_SESSION['Moderateur']['id_user'];
				            	$this->Admin->hydrate($data);
				            	$this->Admin->UpdateAdmin();
				            	$this->User->hydrate($data);
				            	$this->User->UpdateUserinfo();
				          		$this->load->view('ADMIN/index');
								$this->load->view('template_al/navigation');
								$this->load->view('ADMIN/profil');
								$this->load->view('ADMIN/footer');
								$_SESSION['message_save']='modification effective';
								$_SESSION['Moderateur']['nom']=$data['nom'];
								$_SESSION['Moderateur']['prenom']=$data['prenom'];
								$_SESSION['Moderateur']['email']=$data['email'];
								$_SESSION['Moderateur']['profil']=$data['profil'];
				            }else{
				            	redirect(site_url(array('Moderateur','formmodifierprofil')));
				            	$_SESSION['message_error']='!!echec de modification';
				            }
						}else{
							$erreur='non';
							$data['message_save']="L'image choisie  est endommagée  veuillez le remplacer svp !!";
						}
					}else{
						$data['message_save']="La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
						$data['message']='non';
						print_r($_FILES);
					}
				}else{
					print_r($_FILES);
				}
				
			}else{
				redirect(site_url(array('Moderateur','voirprofil')));
			}
		}else{
				session_destroy();
				redirect(site_url(array('Moderateur','formulaireConnexion')));
			}
	}
	}


