<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 

class Moderateur_model extends CI_Model{


		function __construct()
			{
			
			}
	
			// gerer un moderateur

			private $id;
			private $id_user;
			private $email;
			private $password;


			protected $table= 'moderateur';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}

			public function AddModerator(){
			    $this->db->set('email', $this->email)
						->set('password', $this->password)
						->set('id_user', $this->id_user)
						->insert($this->table);
		
			}

			// fonction qui charge toutes infos sur les moderateurs
			public function findAllMod(){
				$data = $this->db->select('id_user,email,password')
								->from($this->table)
								->order_by('id','asc')
								->get()
								->result();
				$i=0;
				$donnees['data'] = 'non';	
				foreach ($data as $row){
					$donnees['cible'][$i] = $this->User->finduserInfos($row->id_user);
					$donnees['identifier'][$i] = $row->id_user;
			       	$i++;
			       	$donnees['data'] = 'ok';
				}
				$donnees['total'] = $i;
				return $donnees;	
			}

            // fonction qui charge toutes les donnees de la table Moderateur
			public function findAllModerateur(){
				$data = $this->db->select('id,id_user,email,password')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i = 0;
				$donnees['data'] = 'non';	
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_user']=$row->id_user;
			       	$donnees[$i]['email']=$row->email;
			       	$donnees[$i]['password']=$row->password;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total'] = $i;
				return $donnees;	
			}


			// fonction qui reccupère les infos d'un moderateur dont l'id est passe en parametre
			public function findModInfos($cible){
				$data = $this->db->select('id_user,email,password')
						->from($this->table)
						->where('id_user', $cible)
						->limit(1)
						->get()
						->result();
				$donnees['data'] = 'non';			
				foreach ($data as $row){
			       	$donnees['id_user'] = $row->id_user;
			       	$donnees['email'] = $row->email;
			       	$donnees['password'] = $row->password;
			       	$donnees['data'] = 'ok';
				}

				return $donnees;
			}

			// fonction pour supprimer un moderteur
			public function DelModerator($cible){
				$this->db->set('email', '')
						->set('password', '')
						->set('id_user', '')
						->where('id_user', $cible['id_user'])
						->delete($this->table);
			}

			// setteurs


				public function setId($id){
					$this->id=$id;
				}


				public function setId_user($id_user){
					$this->id_user=$id_user;
				}
				
				public function setEmail($email){
					$this->email=$email;
				}

				public function setPassword($password){
					$this->password=$password;
				}


			// getteurs

				public function getId(){
					return $this->id;
				
				}

				
				public function getId_user(){
					return $this->id_user;
				
				}

				public function getEmail(){
					return $this->email;
				
				}

				public function getPassword(){
					return $this->password;
				
				}
			 
}

?>
