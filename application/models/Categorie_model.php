<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 

class Categorie_model extends CI_Model{
	


		function __construct()
			{
			
			}

			// gerer une categorie

			private $id;
			private $id_user;
			private $nom_cat;
			private $date_creation;
			private $date_modification;
			private $niveau;
			private $photo_cat;


			protected $table= 'categorie';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}


			public function addCategorie(){

			    $this->db->set('id', $this->id)
			    	->set('id_user', $this->id_user)
			    	->set('nom_cat', $this->nom_cat)
			    	->set('date_creation', $this->date_creation)
			    	->set('niveau', $this->niveau)
			    	->set('photo_cat', $this->photo_cat)
					->insert($this->table);
		
			}




				// fonction qui charge tous les Admins pour faire le filtrage de donnee
			
			public function findAllCategorieBd(){
				$data = $this->db->select('id,id_user,nom_cat,date_creation,date_modification,niveau,photo_cat')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_user']=$row->id_user;
			       	$donnees[$i]['nom_cat']=$row->nom_cat;
			       	$donnees[$i]['date_creation']=$row->date_creation;
			       	$donnees[$i]['date_modification']=$row->date_modification;
			       	$donnees[$i]['niveau']=$row->niveau;
			       	$donnees[$i]['photo_cat']=$row->photo_cat;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}

			public function findAllCategoriemodel(){
	        	$data=$this->db->select('id, id_user, nom_cat, date_creation, date_modification, niveau, photo_cat')
	        			   ->from($this->table)
	        			   ->order_by('id','desc')
	        			   ->get()
	        			   ->result();

	        	$i=0;
	        	$donnees['data']='non';
	        	foreach ($data as $row) {
	        		$donnees[$i]['id']=$row->id;
	        		$donnees[$i]['id_user']=$row->id_user;
	        		$donnees[$i]['nom_cat']=$row->nom_cat;
	        		$donnees[$i]['date_creation']=$row->date_creation;
	        		$donnees[$i]['date_modification']=$row->date_modification;
	        		$donnees[$i]['niveau']=$row->niveau;
	        		$donnees[$i]['photo_cat']=$row->photo_cat;

	        		$donnees['data']='ok';
	        		$i++;
	        	}

	        	$donnees['total']=$i;
	        	return $donnees;
	        }


			// fonction qui reccupère les infos d'une categorie dont l'id est passe en parametre

			public function findcategorieInfos($cible){
				$data = $this->db->select('id,id_user,nom_cat,date_creation,date_modification,niveau,photo_cat')
						->from($this->table)
						->where('id', $cible)
						->limit(1)
						->get()
						->result();

				$donnees['data'] = 'non';			
				foreach ($data as $row){
					$donnees['id'] = $row->id;
					$donnees['id_user'] = $row->id_user;
			       	$donnees['nom_cat'] = $row->nom_cat;
			       	$donnees['date_creation'] = $row->date_creation;
			       	$donnees['date_modification'] = $row->date_modification;
			       	$donnees['niveau'] = $row->niveau;
			       	$donnees['photo_cat'] = $row->photo_cat;
			       	$donnees['data'] = 'ok';
				}
				return $donnees;

            }


            public function UpdateCategorie($cible,$newnom,$newphoto_cat,$date_modification){
				$this->db->set('nom_cat',$newnom)
				        ->set('photo_cat',$newphoto_cat)
				         ->set('date_modification',$date_modification)
            			 ->where('id',$cible)
						 ->update($this->table);
			}



			public function deleteCategorie($cible){
		    	$this->db->where('id',$cible)
		    			->delete($this->table);
		    }

			    //definition des getter et des setter
			   


			   // setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setId_user($id_user){
				$this->id_user=$id_user;
			}
			
			public function setNom_cat($nom_cat){
				$this->nom_cat=$nom_cat;
			}

			public function setDate_creation($date_creation){
				$this->date_creation=$date_creation;
			}

			public function setDate_modification($date_modification){
				$this->date_modification=$date_modification;
			}


			public function setNiveau($niveau){
				$this->niveau=$niveau;
			}


			public function setPhoto_cat($photo_cat){
				$this->photo_cat=$photo_cat;
			}


			// getteurs


			public function getId(){
				$this->id;
			}


			public function getId_user(){
				$this->id_user;
			}
			
			public function getNom_cat(){
				$this->nom_cat;
			}

			public function getDate_creation(){
				$this->date_creation;
			}

			public function getDate_modification(){
				$this->date_modification;
			}


			public function getNiveau(){
				$this->niveau;
			}


			public function getPhoto_cat(){
				$this->photo_cat;
			}
			
}

?>
