<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 

class Mod_Admin_model extends CI_Model{
	


		function __construct()
			{
			
			}
	   // gerer un commentaire 

			private $id;
			private $id_auteur;
			private $id_destinataire;
			private $contenu;
			private $dates;



			protected $table= 'mod_admin';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}


			   //definition des getter et des setter
			   


			   // setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setId_auteur($id_auteur){
				$this->id_auteur=$id_auteur;
			}
			
			public function setId_destinataire($id_destinataire){
				$this->id_destinataire=$id_destinataire;
			}

			public function setContenu($contenu){
				$this->contenu=$contenu;
			}

			public function setDates($dates){
				$this->dates=$dates;
			}



			   //getters
			   
			   

			public function getId(){
				$this->id;
			}


			public function getId_auteur(){
				$this->id_auteur;
			}
			
			public function getId_destinataire(){
				$this->id_destinataire;
			}

			public function getContenu(){
				$this->contenu;
			}

			public function getDates(){
				$this->dates;
			}
			 


}

?>
