<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 

class Theme_model extends CI_Model{


		function __construct()
			{
			
			}
	
			// gerer un theme

			private $id;
			private $id_user;
			private $id_cat;
			private $libelle;
			private $date_creation;
			private $date_modification;
			private $niveau;


			protected $table= 'categorie';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}

			    //definition des getter et des setter
			   


			   // setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setId_user($id_user){
				$this->id_user=$id_user;
			}
			
			public function setId_cat($id_cat){
				$this->id_cat=$id_cat;
			}

			public function setLibelle($libelle){
				$this->libelle=$libelle;
			}

			public function setDate_creation($date_creation){
				$this->date_creation=$date_creation;
			}

			public function setDate_modification($date_modification){
				$this->date_modification=$date_modification;
			}


			public function setNiveau($niveau){
				$this->niveau=$niveau;
			}			   


			   // getters


			public function getId(){
				$this->id;
			}


			public function getId_user(){
				$this->id_user;
			}
			
			public function getId_cat(){
				$this->id_cat;
			}

			public function getLibelle(){
				$this->libelle;
			}

			public function getDate_creation(){
				$this->date_creation;
			}

			public function getDate_modification(){
				$this->date_modification;
			}


			public function getNiveau(){
				$this->niveau;
			}




}

?>
