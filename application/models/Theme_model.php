<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 

class Theme_model extends CI_Model{


		function __construct()
			{
			
			}
	
			// gerer un theme

			private $id;
			private $id_user;
			private $id_cat;
			private $libelle;
			private $date_creation;
			private $date_modification;
			private $niveau;


			protected $table= 'theme';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			
			}

			public function findthemeInfos($id){
				$data =$this->db->select('libelle')
						->from($this->table)
						->where(array('id'=>$id))
						->get()
						->result();

				$donnees['data']='non';			
				foreach ($data as $row){
			       	$donnees['libelle']=$row->libelle;
			       	$donnees['data']='ok';
				}

				return $donnees;
			}

				//ajouter un theme
			public function addTheme(){
				 $this->db->set('id', $this->id)
			    	->set('id_user', $this->id_user)
			    	->set('id_cat', $this->id_cat)
			    	->set('libelle', $this->libelle)
			    	->set('date_creation', $this->date_creation)
			    	->set('niveau', $this->niveau)
					->insert($this->table);
			}


			public function findAllThemeinbd(){
				$data = $this->db->select('id,id_user,id_cat,libelle,date_creation,date_modification')
								->from($this->table)
								->order_by('id','desc') 
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_user']=$row->id_user;
			       	$donnees[$i]['id_cat']=$row->id_cat;
			       	$donnees[$i]['libelle']=$row->libelle;
			       	$donnees[$i]['date_creation']=$row->date_creation;
			       	$donnees[$i]['date_modification']=$row->date_modification;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;
			}







			// public function findAllThemeinbd(){
			//     $data = $this->db->select('id,id_user,id_cat,libelle,date_creation,date_modification')
			// 					->from($this->table)
			// 					->order_by('id','desc')
			// 					->get()
			// 					->result();

			// 	$i=0;
			// 	$donnees['data'] = 'non';	
				
			// 	foreach ($data as $row){
			//        	$donnees[$i]['id']=$row->id;
			//        	$donnees[$i]['id_user']=$row->id_user;
			//        	$donnees[$i]['id_cat']=$row->id_cat;
			//        	$donnees[$i]['libelle']=$row->libelle;
			//        	$donnees[$i]['date_creation']=$row->date_creation;
			//        	$donnees[$i]['date_modification']=$row->date_modification;
			//        	$i++;
			//        	$donnees['data']='ok';
			// 	}
				
			// 	$donnees['total']=$i;
			// 	return $donnees;
		 //    }

		    public function findAllThememodel(){
	        	$data=$this->db->select('id, id_cat, id_user, libelle, date_creation, date_modification, niveau')
	        			   ->from($this->table)
	        			   ->order_by('id','desc')
	        			   ->get()
	        			   ->result();

	        	$i=0;
	        	$donnees['data']='non';
	        	foreach ($data as $row) {
	        		$donnees[$i]['id']=$row->id;
	        		$donnees[$i]['id_cat']=$row->id_cat;
	        		$donnees[$i]['id_user']=$row->id_user;
	        		$donnees[$i]['libelle']=$row->libelle;
	        		$donnees[$i]['date_creation']=$row->date_creation;
	        		$donnees[$i]['date_modification']=$row->date_modification;
	        		$donnees[$i]['niveau']=$row->niveau;

	        		$donnees['data']='ok';
	        		$i++;
	        	}

	        	$donnees['total']=$i;
	        	return $donnees;
	        }

		    public function deleteTheme($cible){
		    	$this->db->where('id',$cible)
		    			->delete($this->table);
		    }


		    	//Rechercher tout les themes qui correspondent a une categorie entree en parametre
		    

		    public function findThemeinbd($cible){
			    $data = $this->db->select('id,id_user,id_cat,libelle,date_creation,date_modification')
								->from($this->table)
								->where('id_cat',$cible)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_user']=$row->id_user;
			       	$donnees[$i]['id_cat']=$row->id_cat;
			       	$donnees[$i]['libelle']=$row->libelle;
			       	$donnees[$i]['date_creation']=$row->date_creation;
			       	$donnees[$i]['date_modification']=$row->date_modification;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;
		    }

		    public function findThemeinfo($cible){
		    	$data = $this->db->select('id,id_cat,id_user,libelle,date_creation,date_modification,niveau')
						->from($this->table)
						->where('id', $cible)
						->limit(1)
						->get()
						->result();

				$donnees['data'] = 'non';			
				foreach ($data as $row){
					$donnees['id'] = $row->id;
					$donnees['id_user'] = $row->id_user;
			       	$donnees['id_cat'] = $row->id_cat;
			       	$donnees['libelle'] = $row->libelle;
			       	$donnees['date_creation'] = $row->date_creation;
			       	$donnees['date_modification'] = $row->date_modification;
			       	$donnees['niveau'] = $row->niveau;
			       	$donnees['data'] = 'ok';
				}
				return $donnees;
		    }


		    public function empechersuppression($cible){
		    	$this->db->set('niveau',4)
            			 ->where('id',$cible)
						 ->update($this->table);
		    }
		    
		    public function UpdateTheme($cible){
				$this->db->set('id_cat',$this->id_cat)
				        ->set('libelle',$this->libelle)
				         ->set('date_modification',$this->date_modification)
            			 ->where('id',$cible)
						 ->update($this->table);
			}





			    //definition des getter et des setter
			   


			   // setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setId_user($id_user){
				$this->id_user=$id_user;
			}
			
			public function setId_cat($id_cat){
				$this->id_cat=$id_cat;
			}

			public function setLibelle($libelle){
				$this->libelle=$libelle;
			}

			public function setDate_creation($date_creation){
				$this->date_creation=$date_creation;
			}

			public function setDate_modification($date_modification){
				$this->date_modification=$date_modification;
			}


			public function setNiveau($niveau){
				$this->niveau=$niveau;
			}			   


			   // getters


			public function getId(){
				$this->id;
			}


			public function getId_user(){
				$this->id_user;
			}
			
			public function getId_cat(){
				$this->id_cat;
			}

			public function getLibelle(){
				$this->libelle;
			}

			public function getDate_creation(){
				$this->date_creation;
			}

			public function getDate_modification(){
				$this->date_modification;
			}


			public function getNiveau(){
				$this->niveau;
			}




}

?>
