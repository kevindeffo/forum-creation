<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 

class Commentaire_model extends CI_Model{
	


			function __construct()
			{
			
			}


			// gerer un commentaire 

			private $id;
			private $id_user;
			private $id_theme;
			private $contenu;
			private $date_creation;
			private $statut;



			protected $table= 'commentaire';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}

			public function findCommentaireInfos($id){
				$data =$this->db->select('contenu')
						->from($this->table)
						->where(array('id'=>$id))
						->get()
						->result();

				$donnees['data']='non';			
				foreach ($data as $row){
			       	$donnees['contenu']=$row->contenu;
			       	$donnees['data']='ok';
				}

				return $donnees;
			}



			public function addCommentaire(){

			    $this->db->set('id', $this->id)
			    	->set('id_theme', $this->id_theme)
			    	->set('id_user', $this->id_user)
			    	->set('contenu', $this->contenu)
			    	->set('date_creation', $this->date_creation)
			    	->set('statut', $this->statut)
					->insert($this->table);
		
			}


			public function findCommentaire($cible){
				$data = $this->db->select('id,id_user,contenu,date_creation,statut')
								->from($this->table)
								->order_by('id','asc')
								->where('id_theme',$cible)
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_user']=$row->id_user;
			       	$donnees[$i]['contenu']=$row->contenu;
			       	$donnees[$i]['date_creation']=$row->date_creation;
			       	$donnees[$i]['statut']=$row->statut;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;
			}

			public function deleteCommentaire($cible){
		    	$this->db->where('id',$cible)
		    			->delete($this->table);
		    }



		    public function findallcommentairebd(){
		    	$data= $this->db->select('id,id_user,contenu,date_creation,statut')
		    			 ->from($this->table)
		    			 ->get()
		    			 ->result();
		    	$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_user']=$row->id_user;
			       	$donnees[$i]['contenu']=$row->contenu;
			       	$donnees[$i]['date_creation']=$row->date_creation;
			       	$donnees[$i]['statut']=$row->statut;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;
		    }


			    //definition des getter et des setter
			   


			   // setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setId_user($id_user){
				$this->id_user=$id_user;
			}
			
			public function setId_theme($id_theme){
				$this->id_theme=$id_theme;
			}

			public function setContenu($contenu){
				$this->contenu=$contenu;
			}

			public function setDate_creation($date_creation){
				$this->date_creation=$date_creation;
			}


			public function setStatut($statut){
				$this->statut=$statut;
			}



			// getteurs


			public function getId(){
				return $this->id;
			}


			public function getId_user(){
				return $this->id_user;
			}
			
			public function getId_theme(){
				return $this->id_theme;
			}

			public function getContenu(){
				return $this->contenu;
			}

			public function getDate_creation(){
				return $this->date_creation;
			}


			public function getStatut(){
				return $this->statut;
			}


}

?>
