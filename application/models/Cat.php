<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 

class Categorie_model extends CI_Model{
	


		function __construct()
			{
			
			}

			// gerer une categorie

			private $id;
			private $id_user;
			private $nom_cat;
			private $date_creation;
			private $date_modification;
			private $niveau;
			private $photo_cat;


			protected $table= 'categorie';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}




			    //definition des getter et des setter
			   


			   // setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setId_user($id_user){
				$this->id_user=$id_user;
			}
			
			public function setNom_cat($nom_cat){
				$this->nom_cat=$nom_cat;
			}

			public function setDate_creation($date_creation){
				$this->date_creation=$date_creation;
			}

			public function setDate_modification($date_modification){
				$this->date_modification=$date_modification;
			}


			public function setNiveau($niveau){
				$this->niveau=$niveau;
			}


			public function setPhoto_cat($photo_cat){
				$this->photo_cat=$photo_cat;
			}


			// getteurs


			public function getId(){
				$this->id;
			}


			public function getId_user(){
				$this->id_user;
			}
			
			public function getNom_cat(){
				$this->nom_cat;
			}

			public function getDate_creation(){
				$this->date_creation;
			}

			public function getDate_modification(){
				$this->date_modification;
			}


			public function getNiveau(){
				$this->niveau;
			}


			public function getPhoto_cat(){
				$this->photo_cat;
			}
			
}

?>
