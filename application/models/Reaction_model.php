<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 

class Reaction_model extends CI_Model{
	

		function __construct()
			{
			
			}
			// gerer une reaction  

			private $id;
			private $id_user;
			private $id_commentaire;
			private $niveau;



			protected $table= 'reaction';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}


			public function Insertion(){

			    $this->db->set('id_user', $this->id_user)
			    	->set('id_commentaire', $this->id_commentaire)
			    	->set('niveau', $this->niveau)
					->insert($this->table);
		
			}


			public function findAllReaction(){
        	$data=$this->db->select('id, id_user, id_commentaire, niveau')
        			   ->from($this->table)
        			   // ->where('niveau', 1)
        			   ->order_by('id','asc')
        			   ->get()
        			   ->result();

        	$i=0;
        	$donnees['data']='non';
        	foreach ($data as $row) {
        		$donnees[$i]['id']=$row->id;
        		$donnees[$i]['id_user']=$row->id_user;
        		$donnees[$i]['id_commentaire']=$row->id_commentaire;
        		$donnees[$i]['niveau']=$row->niveau;
        		$donnees['data']='ok';
        		$i++;
        	}

        	$donnees['total']=$i;
        	return $donnees;
        }




			   //definition des getters et des setters
			   


			   // setters


			public function setId($id){
				$this->id=$id;
			}


			public function setId_user($id_user){
				$this->id_user=$id_user;
			}
			
			public function setId_commentaire($id_commentaire){
				$this->id_commentaire=$id_commentaire;
			}

			public function setNiveau($niveau){
				$this->niveau=$niveau;
			}

			  //getters

			public function getId(){
				$this->id;
			}


			public function getId_user(){
				$this->id_user;
			}
			
			public function getId_commentaire(){
				$this->id_commentaire;
			}

			public function getNiveau(){
				$this->niveau;
			}


}

?>
