<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 

class Signalement_model extends CI_Model{

		function __construct()
			{
			
			}

		// gerer une reaction  

			private $id;
			private $id_user;
			private $id_commentaire;
			private $id_theme;
			private $choix;
			private $statut;

			protected $table= 'signalement';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}


			public function findAllSignalementmodel(){
	        	$data=$this->db->select('id, id_theme, id_commentaire, id_user, choix, statut')
	        			   ->from($this->table)
	        			   ->order_by('id','desc')
	        			   ->get()
	        			   ->result();

	        	$i=0;
	        	$donnees['data']='non';
	        	foreach ($data as $row) {
	        		$donnees[$i]['id']=$row->id;
	        		$donnees[$i]['id_theme']=$row->id_theme;
	        		$donnees[$i]['id_commentaire']=$row->id_commentaire;
	        		$donnees[$i]['id_user']=$row->id_user;
	        		$donnees[$i]['choix']=$row->choix;
	        		$donnees[$i]['statut']=$row->statut;

	        		$donnees['data']='ok';
	        		$i++;
	        	}

	        	$donnees['total']=$i;
	        	return $donnees;
	        }

	        public function findAllSignalementcommentairemodel(){
	        	$data=$this->db->select('id, id_theme, id_commentaire, id_user, choix, statut')
	        			   ->from($this->table)
	        			   ->order_by('id','desc')
	        			   ->get()
	        			   ->result();

	        	$i=0;
	        	$donnees['data']='non';
	        	foreach ($data as $row) {
	        		$donnees[$i]['id']=$row->id;
	        		$donnees[$i]['id_theme']=$row->id_theme;
	        		$donnees[$i]['id_commentaire']=$row->id_commentaire;
	        		$donnees[$i]['id_user']=$row->id_user;
	        		$donnees[$i]['choix']=$row->choix;
	        		$donnees[$i]['statut']=$row->statut;

	        		$donnees['data']='ok';
	        		$i++;
	        	}

	        	$donnees['total']=$i;
	        	return $donnees;
	        }

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}


			public function InsertionSignal($data){

			    $this->db->set('id_user', $data['id_user'])
			    	->set('id_commentaire', $data['id_commentaire'])
			    	->set('id_theme', $data['id_theme'])
			    	->set('choix', $data['choix'])
			    	->set('statut', $data['statut'])
					->insert($this->table);
		
			}



			   //definition des getters et des setters
			   


			   // setters


			public function setId($id){
				$this->id=$id;
			}


			public function setId_user($id_user){
				$this->id_user=$id_user;
			}
			public function setId_theme($id_theme){
				$this->id_theme=$id_theme;
			}
			
			public function setId_commentaire($id_commentaire){
				$this->id_commentaire=$id_commentaire;
			}

			public function setSatut($satut){
				$this->satut=$satut;
			}

			public function setChoix($choix){
				$this->choix=$choix;
			}			   


			   // getters


			public function getId(){
				$this->id;
			}


			public function getId_user(){
				$this->id_user;
			}


			public function getId_theme(){
				$this->id_theme;
			}
			
			public function getId_commentaire(){
				$this->id_commentaire;
			}

			public function getSatut(){
				$this->satut;
			}

			public function getChoix(){
				$this->choix;
			}

}

?>
