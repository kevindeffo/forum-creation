<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 

class Commentaire_model extends CI_Model{
	


			function __construct()
			{
			
			}


			// gerer un commentaire 

			private $id;
			private $id_user;
			private $id_theme;
			private $contenu;
			private $date_creation;
			private $statut;



			protected $table= 'commentaire';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}


			    //definition des getter et des setter
			   


			   // setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setId_user($id_user){
				$this->id_user=$id_user;
			}
			
			public function setId_theme($id_theme){
				$this->id_theme=$id_theme;
			}

			public function setContenu($contenu){
				$this->contenu=$contenu;
			}

			public function setDate_creation($date_creation){
				$this->date_creation=$date_creation;
			}


			public function setStatut($statut){
				$this->statut=$statut;
			}



			// getteurs


			public function getId(){
				$this->id;
			}


			public function getId_user(){
				$this->id_user;
			}
			
			public function getId_theme(){
				$this->id_theme;
			}

			public function getContenu(){
				$this->contenu;
			}

			public function getDate_creation(){
				$this->date_creation;
			}


			public function getStatut(){
				$this->statut;
			}


}

?>
