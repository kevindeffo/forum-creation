<?php echo css('forum-style') ?>


<style>
	
	.cl{
	width: 100px;
	height: 100px;
	border-radius: 50%;
}

.bouttom{
	color: blue;
	border: none;
	background-color: inherit !important;

}
</style>
	<div class="content-wrapper" style="background-color:white;">

		<section class="content-header">
      <h1>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php if(isset($_SESSION['ADMIN'])) { echo site_url(array('Administration','index')); } else { echo site_url(array('Moderateur','index')); } ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Gestion des Categories</a></li>
        <li class="active">Liste des Categorie</li>
      </ol>
    </section>
		<div class="col-md-offset-1 col-md-8 " style="text-align: center;">
			<h2>LISTE DE TOUTES LES CATEGORIES EXISTANTES</h2>
			<div style="color:green;"> <?php if (isset($_SESSION['message_save'])) { echo $_SESSION['message_save'] ;} ?></div>
		</div>
		
		<div class="col-md-2" style="margin-top:15px;"><a href="<?php if(isset($_SESSION['ADMIN'])) { echo site_url(array('Administration','formulaireAddCat'));} else { echo site_url(array('Moderateur','formAddCat')); } ?>" class="btn btn-success"> <span class="glyphicon glyphicon-plus"></span>ajouter une categorie</a></div>
    <!-- Content Header (Page header) -->
    <section class="content">
			<table class=" dataTables_filter table-responsive table-bordered" id="myTable">
				<thead >
					<th>&nbsp</th>
					<th  >Photo-Categorie</th>	
					<th  >Nom-categorie</th>
					<th  >createur</th>
					<th  >Date-creation</th>
					<th>action</th>
				</thead>	
				<tbody>
               <?php for ($i=0; $i<$nom['total'];$i++){ ?> 
						<tr>
							<td class="text-center"><?php echo $i ?></td>		
							<td class="text-center"><?php echo img($nom[$i]['photo_cat'],'cat','cl','ok');?> </td>	
			<td class="text-center">
				<form role="form" action="<?php if(isset($_SESSION['ADMIN'])) { echo site_url(array('Administration','theme_categorie'));} else { echo site_url(array('Moderateur','themeCategorie')); } ?>" method="post">
					<input type="hidden" value="<?php echo $nom[$i]['id'] ?>" name='id'>
					<input type="hidden" value="<?php echo $nom[$i]['nom_cat'] ?>" name='nom_cat'>
					<input type="submit" value="<?php echo $nom[$i]['nom_cat'];?>" class="bouttom">
				</form>
				</td>
				<td  class="text-center"><?php echo $nom[$i]['createur'];   ?> </td> 
				 <td class="text-center"><?php echo $nom[$i]['date_creation'];   ?> </td> 
							 <td class="text-center">
							 		<form action="<?php if(isset($_SESSION['ADMIN'])) { echo site_url(array('Administration','formulairemodifCategorie')); } else { echo site_url(array('Moderateur','FormModifCategorie')); } ?>" method="post" style="display:inline-block;">
							 			<input type="hidden" value="<?php echo $nom[$i]['id'] ?>" name="id">
							 			<button type="submit" title="Editer " class="bouttom"><i class="fa fa-edit"></i> </button> 
							 		</form>
							 		<form action="<?php if(isset($_SESSION['ADMIN'])) { echo site_url(array('Administration','supprimerCategorie')); } else { echo site_url(array('Moderateur','supprCategorie')); } ?>" method="post" style="display:inline-block;">
							 			<input type="hidden" value="<?php echo $nom[$i]['id'] ?>" name="id_cat">
							 			<?php if ($alltheme[$i]['total']==0){ ?>
							 				<button type="submit" title="SUPPRIMER" class="bouttom"><i class="fa fa-trash"></i> </button>
							 				<?php }else{?>
							 					<button type="submit" disabled="true" title="SUPPRIMER" class="bouttom"><i class="fa fa-trash"></i> </button>
							 				<?php } ?>
							 		</form>
							 </td>
						</tr>
				<?php }  ?>
				</tbody>	
			</table>	
   </section>
   <?php 	
			unset($_SESSION['message_save']);
		 ?>
</div>