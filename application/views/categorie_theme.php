<div class="content-wrapper">
	<section class="content-header">
	  <h1>
	    <small></small>
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="<?php if(isset($_SESSION['ADMIN'])) { echo site_url(array('Administration','index')); } else { echo site_url(array('Moderateur','index')); } ?>"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="<?php if(isset($_SESSION['ADMIN'])) { echo site_url(array('Administration','listeCategorie')); } else { echo site_url(array('Moderateur','Categorielist')); } ?>">Liste des Categorie</a></li>
	    <li class="active">Liste des themes</li>
	  </ol>
	</section>
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
        <small></small>
      </h1>
		<ol class="breadcrumb">
        <li><a href="<?php if(isset($_SESSION['ADMIN'])) { echo site_url(array('Administration','index')); } else { echo site_url(array('Moderateur','index')); } ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php if(isset($_SESSION['ADMIN'])) { echo site_url(array('Administration','listeCategorie')); } else { echo site_url(array('Moderateur','listeCategorie')); } ?>">Liste des categories</a></li>
        <li class="active">Liste des theme</li>
      </ol>
	</section>
	<section class="content-header">

		<div class="row">
			<div class="col-md-offset-4 col-md-4">
				<h1>Liste des themes lie a <?php echo $themes['categorie']  ?></h1>
			</div>
		</div>		
	</section>
	<section class="content">
		<table class=" dataTables_filter table-responsive" id="myTable">
			<thead >
				<th>&nbsp</th>
				<th  >Libelle</th>	
				<th  >Date de creation</th>
				<th  >Auteur</th>
				<th>action</th> 
			</thead>	
			<tbody>
				<?php for ($i=0; $i<$themes['taille'];$i++){ ?> 
					<tr>
						<td class="text-center"><?php echo $i ?></td>		
						<td class="text-center"><?php echo $themes[$i]['libelle'] ?></td>	
						<td class="text-center" ><?php echo $themes[$i]['date_creation'] ?></td>	
						<td class="text-center"><?php echo $themes[$i]['nom'] ?></td>
						<td class="text-center">
							<form action="<?php echo site_url(array('Administration','supprimerTheme')) ?>" method="post">
								<input type="hidden" value="<?php echo $themes[$i]['id'] ?>" name="id">
								<button type="submit" title="SUPPRIMER"><i class="fa fa-trash"></i> </button>
							</form>
						</td>	
					</tr>
				<?php }  ?>
				</tbody>	
			</table>	
   </section>
</div>
</div>