
<div class="content-wrapper">
  <section class="content-header">
    <div class="row">
      <div class="col-md-offset-4 col-md-5">
        <h1><?php echo $post['libelle']; ?></h1>
      </div>
    </div>
  </section>
  <section class="content">
    <div class="col-md-offset-4 col-md-5">
      <form role="form" action="<?php if(isset($_SESSION['ADMIN'])){echo site_url(array('Administration','addCommentaire')) ;}else { echo site_url(array('Moderateur','addCommentaire')); } ?>" method="post">
        <div class="box-body">
          <div class="form-group">
            <label >Commentaire</label>
            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Entrez votre reponse" name="comment">
          </div>

          <input type="hidden"  name="id_user"  value="<?php if(isset($_SESSION['ADMIN'])){echo $_SESSION['ADMIN']['id'];}else{ echo $_SESSION['Moderateur']['id_user']; } ?>">
          <input type="hidden" name="auteur" value="<?php if(isset($_SESSION['ADMIN'])){echo $_SESSION['ADMIN']['nom'];}else{ echo $_SESSION['Moderateur']['nom'];} ?>"> 
          <input type="hidden" value="<?php echo date('d/m/y h:i:s') ?>" name="date_creation">
        </div>

        <input type="hidden" name="id_theme" value="<?php echo $post['id']; ?>">
        <input type="hidden" name="niveau" value="1">

        <div class="box-footer" >
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </section>
</div>