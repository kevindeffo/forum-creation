<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="row">
			<div class="col-md-offset-4 col-md-4">
				<h1><?php echo $comment['categorie'].':'.$comment['theme']  ?></h1>
			</div>
		</div>		
	</section>
	<section class="content">
		<table class=" dataTables_filter table-responsive" id="myTable">
			<thead >
				<th  >Libelle</th>	
				<th  >Date de creation</th>
				<th  >Auteur</th>
				<th style="<?php if (isset($_SESSION['Moderateur'])) {
						echo 'display: none';
					} ?>">action</th> 
			</thead>	
			<tbody>
				<?php for ($i=0; $i<$comment['total'];$i++){ ?> 
					<tr>		
						<td ><?php echo $comment[$i]['contenu'] ?></td>	
						<td ><?php echo $comment[$i]['date_creation'] ?></td>	
						<td ><?php echo $comment[$i]['auteur'] ?></td>
						<td style="<?php if (isset($_SESSION['Moderateur'])) {
						echo 'display: none';
					} ?>">
							<form action="<?php echo site_url(array('Administration','supprimerCommentaire')) ?>" method="post" style="display:inline-block;">
							<input type="hidden" value="<?php echo $comment[$i]['id'] ?>" name="id_commentaire">
									<button type="submit"  title="SUPPRIMER" ><i class="fa fa-trash"></i> </button>
							</form>
						</td>	
					</tr>
				<?php }  ?>
			</tbody>	
		</table>	
	</section>
</div>