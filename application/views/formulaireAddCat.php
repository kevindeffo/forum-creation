
<div class="content-wrapper">

  <section class="content-header">
    <div class="box-header with-border" style="text-align: center;">
      <h3 class="box-title">Ajouter une Categorie</h3>
    </div>
  </section>
  <section class="content">
    <div class="col-md-offset-4 col-md-5">
      <div class="box box-primary">
        <form role="form" action="<?php echo site_url(array('Administration','addCategorie')) ?>" method="post" enctype="multipart/form-data">
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Titre</label>
              <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Entrez le titre le la categorie" name="nom_cat">
            </div>
            <div class="form-group" style="margin-top:30px;"> 
              <label for="">Entrez une image qui represente la categorie</label><br>  
              <input type="file" placeholder="Entrez une image qui represente la categorie" name="photo_cat" class="form-control" onchange="loadFile(event)">
            </div> 
          </div>
          <div class="row">
            <div class="col-md-offset-4 col-md-4 " style="margin-bottom: 20px;" >
              <img id="im"  style="width: 100px; height: 100px;">
            </div>
          </div>
          <input type="hidden" name="id_user" value="<?php if(isset($_SESSION['ADMIN'])){ echo $_SESSION['ADMIN']['id_user'];}else{ echo $_SESSION['Moderateur']['id_user'];} ?>">
          <input type="hidden" name="niveau" value="1">

          <div class="box-footer" >
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </section>
</div>
<script type="text/javascript">
  var loadFile = function(event) {
    var profil = document.getElementById('im');
    profil.src = URL.createObjectURL(event.target.files[0]);
  };
</script>