
<!DOCTYPE html>
<html class="translated-ltr" style="height: auto; min-height: 100%;">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> Welcome to Inch Forum </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <style type="text/css">
    .fixe:hover{
      background-color: black !important;
      color: red;
    }
    .bg__first {
       background-image: url(../../../Forum/assets/images/first-bg.jpg );
       background-size: cover;
       background-repeat: no-repeat;
    }
  </style>
   <?php // if (isset($_SESSION['Abonne'])) { ?>
        <?php echo admin_bt_css("css/bootstrap.min"); ?>
        <?php echo admin_dist_css("css/AdminLTE.min"); ?>
        <?php echo admin_dist_css("css/skins/_all-skins.min"); ?>
        <?php echo admin_plugins_css("morris/morris"); ?>
        <?php echo admin_plugins_css("jvectormap/jquery-jvectormap-1.2.2"); ?>
        <?php echo admin_plugins_css("datepicker/datepicker3"); ?>
        <?php echo admin_plugins_css("daterangepicker/daterangepicker"); ?>
        <?php echo admin_plugins_css("bootstrap-wysihtml5/bootstrap3-wysihtml5.min"); ?>
        <script  type=" text/javascript" src="<?php echo base_url().'assets/javascript/jquery-3.6.0.min.js' ?>"></script>

        <?php 
          echo css('app/admin_style'); 
          echo css('app/style'); 
          echo css('font-awesome');
          echo css('ionicons.min'); 
          echo css('bootstrap.min');
          echo css('icon-font.min');
          echo css('table-style');
          echo css('dataTables.bootstrap.min');
          echo css('jquery.dataTables.min');
          echo css('monstyle');
          echo css('forum-style');
        ?>
  <?php //} else { ?>
  <!-- Bootstrap 3.3.7 -->
  <?php echo css('bootstrap.min'); ?>
  <link rel="stylesheet" href="localhost/Forum/assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <?php echo css('font-awesome.min'); ?>
  <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <?php echo css('ionicons.min'); ?>
  <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <?php echo admin_dist_css("css/AdminLTE.min"); ?>
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
       <?php echo admin_dist_css("css/skins/_all-skins.min"); ?>
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<?php// } ?>
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="skin-blue layout-top-nav" style="height: auto; min-height: 100%;">
  <div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top" style="height: 80px; background-color: rgba(0, 0, 0, 0.3);">
      <div class="container">
        <div class="navbar-header">
          <a href="#" class="navbar-brand"> <?php echo img('Logo_header.png','inch forum'); ?> </a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
          </ul>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu" style="padding-top: 20px; position: relative; left: 70px;">
          <?php if (isset($_SESSION['Abonne'])) { ?>
            <ul class="nav navbar-nav">
            <!-- Notifications Menu -->
            <li class="dropdown notifications-menu">
              <!-- Menu toggle button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">dix</font></font></span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">You have 10 notifications</li>
                <li>
                  <!-- Inner Menu: contains the notifications -->
                  <ul class="menu">
                    <li><!-- start notification -->
                      <a href="#">
                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                      </a>
                    </li>
                    <!-- end notification -->
                  </ul>
                </li>
                <li class="footer"><a href="#">View all</a></li>
              </ul>
            </li>
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <?php echo imgProfil($_SESSION['Abonne']['profil'],'user-image','Image utilisateur'); ?>
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><?php echo ($_SESSION['Abonne']['prenom']." ".$_SESSION['Abonne']['nom']); ?></font></font></span>
              </a>
              <ul class="dropdown-menu" style="background-color: rgba(0,0,0,0.8); border: none;">
                <!-- The user image in the menu -->
                <li class="user-header" style="background-color: transparent;">
                  <?php echo imgProfil($_SESSION['Abonne']['profil'],'img-circle','Image utilisateur');  ?>

                  <p>
                    <?php echo ($_SESSION['Abonne']['prenom']." ".$_SESSION['Abonne']['nom']); ?>
                    <small> Abonne IncH Forum </small>
                  </p>
                </li>
                <!-- Menu Body -->
                
                <!-- Menu Footer-->
                <li class="user-footer" style="background-color: rgba(0,0,0,0.8);">
                  <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat" style="background-color: black; color: white;">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?php echo(site_url(array('Abonne', 'deconnexion'))); ?>" class="btn btn-default btn-flat" style="background-color: black; color: white;">Deconnexion</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
         <?php } else { ?>
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <li class="fixe">
              <!-- Menu toggle button -->
              <a  href="<?php echo site_url(array('Welcome','forum')); ?>">
                Forum
              </a>
            </li>
            <!-- /.messages-menu -->

            <!-- Notifications Menu -->
            <li class="fixe">
              <!-- Menu toggle button -->
              <a href="<?php echo site_url(array('Welcome','inscription')); ?>">
                Inscription
              </a>
            </li>

            <!-- Tasks Menu -->
            <li class="fixe">
              <!-- Menu Toggle Button -->
              <a href="<?php echo site_url(array('Abonne','formulaireconnexion')); ?>">
                Connexion
              </a>
            </li>
          </ul>
         <?php } ?>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>

  <!-- Full Width Column -->