<div class="" style="background-color: inherit;">
  <div class="container-fluid">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
      </h1>
      <ol class="breadcrumb"  style="background-color: inherit; font-size: 13px;">
        <li><a href="<?php echo(site_url(array('Welcome','index'))); ?>"><i class="fa fa-dashboard"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Accueil</font></font></a></li>
        <li><a href="<?php echo(site_url(array('Welcome','forum'))); ?>"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Categories</font></font></a></li>
        <li><a href="<?php echo(site_url(array('Welcome','liste_theme'))); ?>"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Themes</font></font></a></li>
        <li class="active"><font style="vertical-align: inherit;"><font style="vertical-align: inherit; color: white;"> Commentaires </font></font></li>
      </ol>
    </section>

  </div>
</div>

<div class="" style="min-height: 200px; background-color: inherit;">
    <div class="container">
      <section class="content" style="margin-bottom: 150px;">
      	<div class="box" style="position: relative; left: 0px; top: 0px; background-color: #c9cdd0; border: 3px solid transparent; width: 800px; margin: auto;">
          <div class="box-header ui-sortable-handle" style="cursor: move;">
            <i class="fa fa-comments-o"></i>

            <h3 class="box-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Commentaires <?php if (isset($_SESSION['Abonne'])) { } else{ echo "<span style=\" color: red ; padding-left: 220px;\">Vous devez vous connecter afin de reagir ! </span>";} ?></font></font></h3>

            <div class="box-tools pull-right" data-toggle="tooltip" title="" data-original-title="Status">
              <div class="btn-group" data-toggle="btn-toggle">
              </div>
            </div>
          </div>
          <div class="slimScroll" style="">
          	<div class="box-body chat" id="chat-box" style="">
            <!-- chat item -->
            <?php for ($i=0; $i <$comment['total'] ; $i++) { ?>
	            <div class="item" style="">
	            		<?php echo imgProfil($createur[$i]['profil'],'offline','image utilisateur','image utilisateur'); ?>
	              <p class="message">
	                <a href="#" class="name">
	                  <small class="text-muted pull-right">
	                  	<i class="fa fa-clock-o"></i>
	                  	<font style="vertical-align: inherit;">
	                  		<font style="vertical-align: inherit;"> 
	                  			<?php echo ' '.$comment[$i]['date_creation']; ?>
	                  		</font>
	                  		<font style="vertical-align: inherit;"> 
		        
	                  		</font>
	                  	</font>
	                  </small>
	                  <font style="vertical-align: inherit;">
	                  	<font style="vertical-align: inherit;"><?php echo ($createur[$i]['nom'].' '.$createur[$i]['prenom']); ?> </font>
	                  </font>
	                </a>
	                <font style="vertical-align: inherit;">
	                	<font style="vertical-align: inherit;"> <?php echo $comment[$i]['contenu']; ?></font>
	                <font style="vertical-align: inherit;">
	             		</font></font>
	            	</p>
							</div>

						  <div class="item" style=" height: 20px;">
						 	 <?php //if (isset($_SESSION['Abonne'])) { ?>
						 	 	<div class="pull-right" style="position: relative; bottom: 30px;">
										<form action="<?php echo site_url(array('Welcome','SignalerComment')) ?>" method="post" style=" display: inline-block; border: none;">
											<input type="hidden" value="<?php echo $comment[$i]['id'];?>" name="id_comment" >
											<input type="hidden" value="<?php echo $theme['id'] ?>" name="id_theme" >
											<input type="hidden" value="<?php if (isset($_SESSION['Abonne'])) { echo $_SESSION['Abonne']['id']; } ?>"  name="id" >
											<input type="hidden" value="<?php echo $theme['id']; ?>" name="id_theme" >
											<input type="hidden" value="<?php echo $theme['id_cat']; ?>" name="id_cat" >
											<button type="submit" style="border: none;background-color: inherit;"<?php if(isset($_SESSION['Abonne'])){ echo(" ");}else { echo ('disabled="disabled"');} ?> title="<?php if(isset($_SESSION['Abonne'])){ echo(" ");}else { echo ('Veillez vous connecter afin d\'interagir sur le forum ');} ?>"> <span style="color: red; font-size: 10px;" title="signaler"><i class="fa fa-warning"></i></span></button>
										</form>
										<form action="<?php echo site_url(array('Welcome','LikeComment')) ?>" method="post" style=" display: inline-block;">
											<input type="hidden" value="<?php if (isset($_SESSION['Abonne'])) { echo $_SESSION['Abonne']['id']; } ?>" name="id_user" >
											<input type="hidden" value="<?php echo $comment[$i]['id']; ?>" name="id_comment" >
											<input type="hidden" value="<?php echo $theme['id']; ?>" name="id_theme" >
											<input type="hidden" value="<?php echo $theme['id_cat']; ?>" name="id_cat" >
											<button type="submit" style="border: none;background-color: inherit;" <?php if(isset($_SESSION['Abonne'])){ echo(" ");}else { echo ('disabled="disabled"');} ?> title="<?php if(isset($_SESSION['Abonne'])){ echo(" ");}else { echo ('Veillez vous connecter afin d\'interagir sur le forum ');} ?>"> <span style="color: blue; font-size: 10px;" title="j'aime"> <i class="glyphicon glyphicon-thumbs-up"></i> </span> <span style="font-weight: bolder; color: grey;"> <?php $like=0; for($j=0;$j<count($reaction)-2;$j++){ if( ($comment[$i]['id']==$reaction[$j]['id_commentaire']) && ($reaction[$j]['niveau']==1)){ $like++; }} echo $like; ?></span> </button>
										</form>
										<form action="<?php echo site_url(array('Welcome','UnLikeComment')) ?>" method="post" style=" display: inline-block;">
											<input type="hidden" value="<?php if (isset($_SESSION['Abonne'])) { echo $_SESSION['Abonne']['id']; } ?>" name="id_user" >
											<input type="hidden" value="<?php echo $comment[$i]['id']; ?>" name="id_comment" >
											<input type="hidden" value="<?php echo $theme['id']; ?>" name="id_theme" >
											<input type="hidden" value="<?php echo $theme['id_cat']; ?>" name="id_cat" >
											<button type="submit" style="border: none;background-color: inherit;" <?php if(isset($_SESSION['Abonne'])){ echo(" ");}else { echo ('disabled="disabled"');} ?> title="<?php if(isset($_SESSION['Abonne'])){ echo(" ");}else { echo ('Veillez vous connecter afin d\'interagir sur le forum ');} ?>"> <span style="color: orange; font-size: 10px;" title="je n'aime pas"> <i class="glyphicon glyphicon-thumbs-down"></i></span> <span style="font-weight: bolder; color: grey;"> <?php $likeinv=0; for($j=0;$j<count($reaction)-2;$j++){ if( ($comment[$i]['id']==$reaction[$j]['id_commentaire']) && ($reaction[$j]['niveau']==2)){ $likeinv++; }} echo $likeinv; ?></span></button>
										</form>
								</div>
								<?php// } ?>
						  </div>
            <?php } ?>
            <!-- /.item -->
          </div>
          <div class="slimScrollBar" style="">
          	
          </div>
          <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;">
          	
          </div>
        </div>
          <!-- /.chat -->
          <div class="box-footer" style="background-color: #c9cdd0; border: inherit;">
          	<form action="<?php echo site_url(array('Welcome','liste_commentairetheme')); ?>" method="post">
	            <div class="input-group">
	            	<input type="hidden" value="<?php echo $theme['id']; ?>" name="id_theme" >
								<input type="hidden" value="<?php echo date('Y-m-d_H-i'); ?>" name="date_creation" >
								<input type="hidden" value="1" name="niveau" >
								<input type="hidden" value="2" name="Selecteur">
								<input type="hidden" value="<?php echo $id_cat; ?>" name="id_cat" >
	              <input class="form-control" name="comment" placeholder="Tapez votre message...">
								<?php if(isset($_SESSION['Abonne'])) { ?><input type="hidden" value="<?php echo $_SESSION['Abonne']['id']; ?>" name="id_user" > <?php } ?>
	              <div class="input-group-btn">
	                <a href="<?php echo site_url(array('Welcome','liste_commentairetheme')) ?>"><button type="submit" class="btn btn-success"<?php if(isset($_SESSION['Abonne'])){ echo(" ");}else { echo ('disabled="disabled"');} ?> title="<?php if(isset($_SESSION['Abonne'])){ echo(" ");}else { echo ('Veillez vous connecter afin d\'interagir sur le forum ');} ?>"> Envoyer </button> </a>
	              </div>
	            <!-- </div> -->
            </form>
          </div>
          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
</div>