<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Welcome to Inch forum </title>
  <link href="//fonts.googleapis.com/css2?family=Barlow:ital,wght@0,300;0,600;0,700;1,400&display=swap" rel="stylesheet">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="icon" type="image/png" href="<?php echo img_url('logot.png') ?>" />
  <?php echo admin_bt_css("css/bootstrap.min"); ?>
  <?php echo admin_dist_css("css/AdminLTE.min"); ?>
  <?php echo admin_dist_css("css/skins/_all-skins.min"); ?>
  <?php echo admin_plugins_css("morris/morris"); ?>
  <?php echo admin_plugins_css("jvectormap/jquery-jvectormap-1.2.2"); ?>
  <?php echo admin_plugins_css("datepicker/datepicker3"); ?>
  <?php echo admin_plugins_css("daterangepicker/daterangepicker"); ?>
  <?php echo admin_plugins_css("bootstrap-wysihtml5/bootstrap3-wysihtml5.min"); ?>
  <script  type=" text/javascript" src="<?php echo base_url().'assets/javascript/jquery-3.6.0.min.js' ?>"></script>

    <script>
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //Meta tag Keywords -->

    <!-- Custom-Files -->
    <?php 
      echo  css('bootstrap.min'); 
    ?>
    <!-- Bootstrap-Core-CSS -->
    <?php 
      echo  css('Acceuil/simpleLightbox'); 
      echo  css('Acceuil/roadmap'); 
    ?>
    <!-- Banner-Slider-CSS -->
    <?php 
      echo  css('Acceuil/snow'); 
      echo  css('Acceuil/aos'); 
      echo  css('Acceuil/aos-animation'); 
      echo  css('Acceuil/style'); 
      echo  css('Acceuil/fontawesome-all'); 
      echo css('forum-style');
    ?>
    <!-- /Fonts -->
    <link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,400,400i,500,500i,700,700i,900" rel="stylesheet">

  <?php 
    echo css('app/admin_style'); 
    echo css('app/style'); 
    echo css('font-awesome');
    echo css('ionicons.min'); 
    echo css('bootstrap.min');
    echo css('bootstrap');
    echo css('icon-font.min');
    echo css('table-style');
    echo css('dataTables.bootstrap.min');
    echo css('jquery.dataTables.min');
    echo css('monstyle');
    echo css('fontawesome-all.min');
    echo css('forum-style');
    echo css('style');
    echo css('menufullpage');
    
    
    
  ?>
  
 
</head>

<body>
    <div class="container-fluid">
        <header>
		
			<nav class="navbar navbar-expand-lg navbar-light" style="    background-color: skyblue;" >
				<a class="navbar-brand" style="margin-top:-20px !important;" href="<?php echo site_url(array('Welcome','index')) ?>">
					Inch Forum
				</a>
				<div class="pull-right">
					<ul class="navbar-nav text-center ml-auto" style="margin-left: 700px;">
                        <li class="nav-item active btn btn-outline-light btn-sm " style="height: 35px !important; margin-left: 10px;">
                            <a class="" href="<?php echo site_url(array('Welcome','index')) ?>">Home
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
						<li class="nav-item btn btn-outline-light btn-sm" style="height: 35px !important; margin-left: 10px;" >
							<a class=" " href=" <?php echo site_url(array('Welcome','inscription')) ?>" >Inscription</a>
						</li>
						<li class="nav-item dropdown btn btn-outline-light btn-sm" style="height: 35px !important; margin-left: 10px;">
							<a class=" dropdown-toggle  " href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
							    aria-expanded="false" style="margin-left: 10px;">
								Connexion
							</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdown" style="width: 300px !important; height: 155px !important; margin-left: -140px; position: absolute;">
							     <form method="POST" action="<?php echo site_url(array('Abonne','traitementConnexion')); ?>">
									    <input type="email" name="email" placeholder="veuillez entrer votre adresse mail" id="mail" required="" class="form-control" >
                			<input type="password" name="password" placeholder="Veuillez entrer votre mot de passe" id="pwd"  required="" class="form-control" style="margin-top: 10px;"><br>
                			<input type="submit" value="Envoyer" class="pull-right btn btn-block btn-primary"><br><br>
							     </form>
                            </div>
						</li>
					</ul>
				</div>
			</nav>
		</header>
	