<div class="content-wrapper" style="background-color: inherit;">
    <div class="container-fluid">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
        </h1>
        <ol class="breadcrumb"  style="background-color: inherit; font-size: 13px;">
          <li><a href="<?php echo(site_url(array('Welcome','index'))); ?>"><i class="fa fa-dashboard"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Accueil</font></font></a></li>
          <li class="active"><font style="vertical-align: inherit;"><font style="vertical-align: inherit; color: white;"> Forum </font></font></li>
        </ol>
      </section>

    </div>
  </div>

<?php echo css('styleee'); ?>
<link href='//fonts.googleapis.com/css?family=Carrois+Gothic+SC' rel='stylesheet' type='text/css'>
<div class="wrap">
    <div class="about">
        <div class="clear"> </div>
        <div class="clear"> </div>
        <div class="clear"> </div>
        <h2 style="color: white; margin-bottom: 20px;">CATEGORIES</h2>
        <?php for ($i=0; $i<$allcat['total'];$i++){ ?>
                <div class="col-sm-4">
                    <form  action="<?php echo site_url(array('Welcome','liste_theme')) ?>" method="post"  style="display: inline-block;"> 
                        <input type="hidden" value="<?php echo $allcat[$i]['id'] ?>" name="id_cat">
                        <input type="hidden" value="<?php echo $allcat[$i]['nom_cat'] ?>" name="nom_cat">
                        <input type="hidden" value="<?php echo $allcat[$i]['photo_cat'] ?>" name="photo_cat">
                        <button type="submit" style="border: none;">
                            <div class="content-gallery">
                                <div class="boxgrid caption">
                                    <?php echo img($allcat[$i]['photo_cat']); ?>
                                    <div class="boxcaption" style="border-radius: 330px; padding-top: 20px;">
                                        <h3 style="text-align: center;"><?php echo $allcat[$i]['nom_cat'] ?></h3>
                                    </div>
                                </div>
                            </div>
                        </button> 
                    </form>  
                </div>
        <?php }  ?>
        <div class="clear"> </div>
    </div>
</div>
<div class="clear"> </div>
<div class="wrap">
    <ul class="dc_pagination dc_paginationA dc_paginationA03">
      <!-- <li><a href="#" class="first">Voir Toutes Les Categories</a></li> -->
    </ul>
    <div class="clear"> </div>
</div>