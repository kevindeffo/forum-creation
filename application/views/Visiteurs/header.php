<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Welcome to Inch forum </title>
  <link href="//fonts.googleapis.com/css2?family=Barlow:ital,wght@0,300;0,600;0,700;1,400&display=swap" rel="stylesheet">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <?php echo admin_bt_css("css/bootstrap.min"); ?>
  <?php echo admin_dist_css("css/AdminLTE.min"); ?>
  <?php echo admin_dist_css("css/skins/_all-skins.min"); ?>
  <?php echo admin_plugins_css("morris/morris"); ?>
  <?php echo admin_plugins_css("jvectormap/jquery-jvectormap-1.2.2"); ?>
  <?php echo admin_plugins_css("datepicker/datepicker3"); ?>
  <?php echo admin_plugins_css("daterangepicker/daterangepicker"); ?>
  <?php echo admin_plugins_css("bootstrap-wysihtml5/bootstrap3-wysihtml5.min"); ?>
  <script  type=" text/javascript" src="<?php echo base_url().'assets/javascript/jquery-3.6.0.min.js' ?>"></script>

    <script>
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //Meta tag Keywords -->

    <!-- Custom-Files -->
    <?php 
      echo  css('bootstrap.min'); 
    ?>
    <!-- Bootstrap-Core-CSS -->
    <?php 
      echo  css('Acceuil/simpleLightbox'); 
      echo  css('Acceuil/roadmap'); 
    ?>
    <!-- Banner-Slider-CSS -->
    <?php 
      echo  css('Acceuil/snow'); 
      echo  css('Acceuil/aos'); 
      echo  css('Acceuil/aos-animation'); 
      echo  css('Acceuil/style'); 
      echo  css('Acceuil/fontawesome-all'); 
      echo css('forum-style');
    ?>
    <!-- /Fonts -->
    <link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,400,400i,500,500i,700,700i,900" rel="stylesheet">

  <?php 
    echo css('app/admin_style'); 
    echo css('app/style'); 
    echo css('font-awesome');
    echo css('ionicons.min'); 
    echo css('bootstrap.min');
    echo css('bootstrap');
    echo css('icon-font.min');
    echo css('table-style');
    echo css('dataTables.bootstrap.min');
    echo css('jquery.dataTables.min');
    echo css('monstyle');
    echo css('fontawesome-all.min');
    echo css('forum-style');
    echo css('style');
    echo css('menufullpage');
    echo css('Abonne/css/style_A');
    
    
    
  ?>

  <link rel="icon shortcut" type="image/png" href="<?php echo img_url('logo.png') ?>" >
</head>

<body>

<header class="container-fluid" style="background-color:black; opacity: 0.7;">
    <div class="row">
        <div class="col-md-4 ">
            <h1 class="animate__animated animate__bounceInDown title_h1" style="margin-top:10px !important;"> <span style="color:blue;">Inch</span> Forum</h1>
        </div>
        <div class="col-md-8 ">
            <nav class="navbar navbar-default" id="nav_div_1">
                <button type="button" class=" navbar-toggle text-center" id="but1" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <ul class=" nav    navbar-collapse" id="nav_ul" style="margin-top:10px !important;">
                    <form method="POST" action="">
                        <li class="input-group">
                            <input type="text" class="form-control" name="recherche" placeholder=" Entrer un indice" style="height: 26px !important;" >
                            <span class="input-group-btn" style="margin-right: 40px;">
                                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span>
                                </button>
                            </span>
                        </li>
                    </form>
                    <li><a id="ancre" href="<?php echo site_url(array('Welcome','inscription')) ?>">Inscription</a></li>
                    <li class="dropdown user-menu">
                        <a href="#" id="ancre" class="dropdown-toggle" data-toggle="dropdown">Connexion
                        </a>
                        <div class="dropdown-menu menu_profil" style="margin-left:-74px; width:250px;">
                            <form method="POST" action="<?php echo site_url(array('Abonne','traitementConnexion')); ?>" >
                                <input type="email" name="email" class="form-control" placeholder="Entrer votre adresse Email">
                                <input type="password" name="password" class="form-control" placeholder="Entrer votre mot de passe" style="margin-top:8px;">
                                <input type="submit" value="Envoyer" class="form-control btn btn-primary" style="margin-top:6px;">
                            </form>
                        </div>
                    </li>
                </ul>
            </nav>
        </div>
        
    </div>
</header>
	