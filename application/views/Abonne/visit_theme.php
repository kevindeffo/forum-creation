
	<?php      
		echo(css('bootstrap.min')); 
		echo(css('forum-style')); 
		echo(css('font-awesome')); 
		echo(css('icon-font.min')); 
		echo(css('ionicons.min')); 
	?>



<body>

		 <section class="content-header">
            <ol class="breadcrumb" style="padding-left: 120px; padding-top: 15px;">
              <li><a href="<?php echo site_url(array('Welcome','index')); ?>"><i class="fa fa-dashboard">Acceuil </i></a></li>
              <li> <a href="<?php 	echo site_url(array('Welcome','forum')) ?>">Catégories</a></li>
              <li> <a  type="button" onclick = "history.back()">Themes</a></li>
              <li>Commentaires</li>
            </ol>
        </section>
		<div class="container" style="border-radius: 5px;">
			<?php for ($i=0; $i <$comment['total'] ; $i++) { ?>
			<div class="row" style="padding-top: 20px; padding-bottom: 20px; background-color: #ececec; border-bottom: 3px solid black; width: 1000px; margin: auto;">
				<div class="col-md-2 text-center">
					<p style="font-weight: bold; color: green;"> <?php echo ($createur[$i]['nom'].' '.$createur[$i]['prenom']); ?></p>
					<p></p>
					<p></p>

					<?php echo imgProfil($createur[$i]['profil'],'cl img-circle','photo','photo'); ?>
					<p></p>
					<p></p>
					<p class="" style="font-size: 13px;"> posté le : <?php echo ' '.$comment[$i]['date_creation']; ?></p>
				</div>
				<div class="col-md-offset-1 col-md-9" style="text-align: left;">
					<p style="margin-bottom:30px;"> <?php echo $comment[$i]['contenu']; ?> </p>
				</div>
				<div class="col-md-offset-10 col-md-2 pull pull-right">
					<div>
						<?php if (isset($_SESSION['Abonne'])) { ?>
						<form action="<?php echo site_url(array('Welcome','SignalerComment')) ?>" method="post" style=" display: inline-block;">
							<input type="hidden" value="<?php echo $comment[$i]['id'];?>" name="id_comment" >
							<input type="hidden" value="<?php echo $theme['id'] ?>" name="id_theme" >
							<input type="hidden" value="<?php echo $_SESSION['Abonne']['id'];?>"  name="id" >
							<input type="hidden" value="<?php echo $theme['id']; ?>" name="id_theme" >
							<input type="hidden" value="<?php echo $theme['id_cat']; ?>" name="id_cat" >
							<button type="submit" style="border: none;"> <span style="color: red; font-size: 15px;" title="signaler"><i class="fa fa-warning"></i></span></button>
						</form>
						<form action="<?php echo site_url(array('Welcome','LikeComment')) ?>" method="post" style=" display: inline-block;">
							<input type="hidden" value="<?php echo $_SESSION['Abonne']['id'];?>" name="id_user" >
							<input type="hidden" value="<?php echo $comment[$i]['id']; ?>" name="id_comment" >
							<input type="hidden" value="<?php echo $theme['id']; ?>" name="id_theme" >
							<input type="hidden" value="<?php echo $theme['id_cat']; ?>" name="id_cat" >
							<button type="submit" style="border: none;" > <span style="color: blue; font-size: 15px;" title="j'aime"> <i class="glyphicon glyphicon-thumbs-up"></i> </span> <span style="font-weight: bolder; color: grey;"> <?php $like=0; for($j=0;$j<count($reaction)-2;$j++){ if( ($comment[$i]['id']==$reaction[$j]['id_commentaire']) && ($reaction[$j]['niveau']==1)){ $like++; }} echo $like; ?></span> </button>
						</form>

						<form action="<?php echo site_url(array('Welcome','UnLikeComment')) ?>" method="post" style=" display: inline-block;">
							<input type="hidden" value="<?php echo $_SESSION['Abonne']['id'];?>" name="id_user" >
							<input type="hidden" value="<?php echo $comment[$i]['id']; ?>" name="id_comment" >
							<input type="hidden" value="<?php echo $theme['id']; ?>" name="id_theme" >
							<input type="hidden" value="<?php echo $theme['id_cat']; ?>" name="id_cat" >
							<button type="submit" style="border: none;" > <span style="color: orange; font-size: 15px;" title="je n'aime pas"> <i class="glyphicon glyphicon-thumbs-down"></i></span> <span style="font-weight: bolder; color: grey;"> <?php $likeinv=0; for($j=0;$j<count($reaction)-2;$j++){ if( ($comment[$i]['id']==$reaction[$j]['id_commentaire']) && ($reaction[$j]['niveau']==2)){ $likeinv++; }} echo $likeinv; ?></span></button>
						</form>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="row" style="height: 20px;"></div>
			<?php } ?>
			<div class="row" style="width: 1000px; margin: auto;">
				<div class="col-md-offset-1" style="margin-top: 50px; margin-bottom: 50px;">
					<h4> Poster votre commentaire ici <span style="padding:15px; color: #FA9071;"> <i class="glyphicon glyphicon-hand-down"></i> </span> </h4>
					<p></p>	
					<p></p>
					<form action="<?php echo site_url(array('Welcome','liste_commentairetheme'));//echo site_url(array('Abonne','ajoutCommentaire')); ?>" method="post">
						<textarea rows="10" cols="80"  id="local-upload" name="comment"></textarea>
						<input type="hidden" value="<?php echo $theme['id']; ?>" name="id_theme" >
						<input type="hidden" value="<?php echo date('Y-m-d_H-i'); ?>" name="date_creation" >
						<input type="hidden" value="1" name="niveau" >
						<?php if(isset($_SESSION['Abonne'])) { ?><input type="hidden" value="<?php echo $_SESSION['Abonne']['id']; ?>" name="id_user" > <?php } ?>
						<input type="hidden" value="2" name="Selecteur" >
						<input type="hidden" value="<?php echo $id_cat; ?>" name="id_cat" >
						<input type="submit" class="btn btn-primary" value="Répondre">
					</form>
				</div>
			</div>
		</div>
		<!-- <script src="https://cdn.tiny.cloud/1/5r3678jn7snbdpw0u4zvt2zth82bm2nwio7sula0k37hnrp0/tinymce/5/tinymce.min.js"></script> -->
		<!-- <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
		<script type="text/javascript">
	    var base_url = {
	      'url' : 'http://localhost/k-immofinanz/',
	      'author' : 'Cyprien DONTSA'
	    };
	    var finalUrl = base_url.url+'Administration/upload';
	    tinymce.init({
	      	selector: 'textarea#local-upload',
	      	// plugins: 'image code searchreplace wordcount visualblocks visualchars fullscreen insertdatetime media nonbreaking contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker codesample',
	      	// toolbar1: 'undo redo | newdocument | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect',
        // 	toolbar2: "cut copy paste  | searchreplace | bullist numlist | outdent indent | link unlink   | insertdatetime preview | forecolor backcolor | table | hr removeformat | subscript superscript   |  fullscreen | ltr rtl | image code |codesample print | contextmenu",

	      /* without images_upload_url set, Upload tab won't show up*/
	      	images_upload_url: 'finalUrl',

	      /* we override default upload handler to simulate successful upload*/
	      	images_upload_handler: function (blobInfo, success, failure) {
		        var xhr, formData;
		        
		        xhr = new XMLHttpRequest();
		        xhr.withCredentials = false;
		        xhr.open('POST',finalUrl);
		      
		        xhr.onload = function() {
		            var json;
		        
		            if (xhr.status != 200) {
		                failure('HTTP Error: ' + xhr.status);
		                return;
		            }
		        
		            json = JSON.parse(xhr.responseText);
		        
		            if (!json || typeof json.location != 'string') {
		                failure('Invalid JSON: ' + xhr.responseText);
		                return;
		            }
		        
		            success(json.location);
		        };
		      
		        formData = new FormData();
		        formData.append('file', blobInfo.blob(), blobInfo.filename());
		      
		        xhr.send(formData);
		    },
	    });
  	</script> -->
  	<?php echo js('scriptcomment'); ?>
</body>
</html>
<!-- background-color: #ececec;  #E5F0CB  -->