<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title> Welcome to Inch Forum </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
	     <?php  
          echo css('bootstrap.min');
          echo css('font-awesome.min');
          echo css('wow-animate');
          echo css('templatemo-style');
        ?>
        <style type="text/css">
        	.fixe:hover{
        		background-color: black !important;
        		color: red;
        	}
        </style>
    </head>
    <body>
        <div class="row">
          <div class="header" id="top">
              <nav class="navbar" role="navigation" style="background-color: black; opacity: 0.8;">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </button>
                        <a href="#" class="navbar-brand scroll-top">
                          <?php echo img('logo.png','inch forum'); ?>
                        </a>
                    </div>
                    <div id="main-nav" class="collapse navbar-collapse">
                      <ul class="nav navbar-nav navbar-right">
                        <li><a class="fixe" href="<?php echo site_url(array('Welcome','forum')); ?>"> <span class="fixe" style="background-color: black; border: 1px solid white; border-radius: 5px; margin-top: 15px;"> Forum </span> </a></li>
                        <li><a href="#"><span class="fixe" style="background-color: black; border: 1px solid white; border-radius: 5px; margin-top:0px;" >   <?php if (isset($_SESSION['Abonne'])) {?>
                            <span style="border-radius:50%; height:70px !important; width: 70px !important;"><?php echo img($_SESSION['Abonne']['profil'],'img_profil','img_profil','dde') ?></span>
                            <?php echo $_SESSION['Abonne']['nom']."  ".$_SESSION['Abonne']['prenom'] ?></span></a>
                            <ul class="sub-menu">
                              <li><a href="">Modifier profil</a><a style="margin-left: 40px;" href="<?php echo site_url(array('Abonne','deconnexion'))?>">Se deconnecter</a></li>
                            </ul>
                        </li>
                        
                      <?php } ?>
                      </ul>
                    </div>
                    </div>
            </nav>
          </div>
        </div>