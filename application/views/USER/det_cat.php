<!DOCTYPE html>
<html>
<head>
	<title>details des categories</title>
	<meta charset="utf-8">
		<?php 
			echo css('bootstrap.min'); 
			echo css('det_cat'); 
			echo css('font-awesome'); 
		?>
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
</head>
<link rel="icon" type="image/png" href="<?php echo img_url('logo.PNG') ?>" />
<body>
	<div class="container cont">
		<div class="row row1">
			<div class="col-md-12 col-sm-12 col-xs-12 col0"></div>
			<div class="col-md-12  col-sm-12 col-xs-12 col1">
				<a href=""><span class="glyphicon glyphicon-chevron-left gly1">Retour aux categories</span></a>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 col2"></div>
			<div class="col-md-3 col-sm-3 col-xs-3 col1 col3">
				<img src="../assets/images/html.png" class="img-circle">
			</div>
			<div class="col-md-9 col-sm-9 col-xs-9 col4" >
				<h2 class="h_1">HTML</h2>
				<p class="p1">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>		
		</div>
		<div class="row row2">
			<div class="col-md-12 col-sm-12 col-xs-12 col0"></div>
			<div class="col-md-12  col-sm-12 col-xs-12 col">
				<a href="">
					<div class="row">
					 	<div class="col-md-offset-1 col-sm-offset-1 col-xs-offset-1 col-md-1 col-sm-1 col-xs-1 col5">
							<img src="../assets/images/balise.jpg" class="img-circle img">
						</div>
						<div class="col-md-offset-1 col-sm-offset-1 col-xs-offset-1 col-md-5 col-sm-5 col-xs-5">
							<h2>les balises html</h2>
						</div>
					</div>
				</a>
				
			</div>
		</div>
	</div>


	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"> </script>
</body>
</html>