<!DOCTYPE html>
<head>
	<title>Login To Inch Forum </title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php 
		echo css('bootstrap.min'); 
		echo css('bootstrap-theme.min'); 
		echo css('bootstrap-social'); 
		echo css('font-awesome.min'); 
		echo css('templatemo1_style'); 
	?> 
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap-social.css" rel="stylesheet" type="text/css">	
	<link href="css/templatemo_style.css" rel="stylesheet" type="text/css">
	<link rel="icon" type="image/png" href="<?php echo img_url('logo.PNG') ?>" />	
</head>
<body class="templatemo-bg-image-12" style="background-color:#d2d6de;">
	<div class="container">
		<div class="col-md-12">			
			<form enctype="multipart/form-data" class="form-horizontal templatemo-login-form-2" role="form" action=" <?php echo site_url(array('Abonne','traitementConnexion')); ?> " method="post" style="border: 4px solid white;">
				<div class="row">
					<div class="col-md-12">
						<h1>Connexion</h1>
						<input type = "button" value = "x" style=" float: right; margin-top: -75px;"  onclick = "history.back()">
					</div>
				</div>
				<div class="row">
					<div class="templatemo-one-signin col-md-6">
				        <div class="form-group">
				          <div class="col-md-12">		          	
				            <label for="email" class="control-label">Email</label>
				            <div class="templatemo-input-icon-container">
				            	<i class="fa fa-user"></i>
				            	<input type="text" class="form-control" id="username" name="email" placeholder="">
				            </div>		            		            		            
				          </div>              
				        </div>
				        <div class="form-group">
				          <div class="col-md-12">
				            <label for="password" class="control-label">Password</label>
				            <div class="templatemo-input-icon-container">
				            	<i class="fa fa-lock"></i>
				            	<input type="password" class="form-control" id="password" name="password" placeholder="">
				            </div>
				          </div>
				        </div>
				        <div class="form-group">
				          <div class="col-md-12">
				            <div class="checkbox">
				                <label>
				                  <input type="checkbox"> se souvenir de moi
				                </label>
				            </div>
				          </div>
				        </div>
				        <div class="form-group">
				          <div class="col-md-12">
				            <input type="submit" value="Connexion" class="btn btn-success">
				          </div>
				        </div>
				        <div class="form-group">
				          	<div class="col-md-12">
				        		<a href="forgot-password.html" class="text-center">probleme de connexion?</a>
				       	 	</div>
				    	</div>
					</div>
					<div class="templatemo-other-signin col-md-6">
						<a class="btn btn-block btn-social btn-facebook margin-bottom-15">
						    <i class="fa fa-facebook"></i> Se connecter avec Facebook
						</a>
						<a class="btn btn-block btn-social btn-twitter margin-bottom-15">
						    <i class="fa fa-twitter"></i> Se connecter avec Twitter
						</a>
						<a class="btn btn-block btn-social btn-google-plus">
						    <i class="fa fa-google-plus"></i> Se connecter avec Google
						</a>
					</div>   
				</div>				 	
		    </form>		      		      
		</div>
	</div>
</body>
</html>