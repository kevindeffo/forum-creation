<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header text-center"> Tableau de Bord </li>
      <li style="<?php if (isset($_SESSION['ADMIN'])) { echo ("display: block;");  } else{ echo ("display: none;");  } ?>"><a href="<?php echo site_url(array('Administration','index')); ?>"><i class="fa fa-dashboard text-aqua"></i> <span> Acceuil </span></a></li>
      <li style="<?php if (isset($_SESSION['Moderateur'])) { echo ("display: block;");  } else{ echo ("display: none;");  } ?>"><a href="<?php echo site_url(array('Moderateur','index')); ?>"><i class="fa fa-dashboard text-aqua"></i> <span> Acceuil </span></a></li>
      <?php if (isset($_SESSION['ADMIN'])) { ?>
      <li class="treeview  menu-open" style="<?php if (isset($_SESSION['ADMIN'])) { echo ("display: block;");  } else{ echo ("display: none;");  } ?>">
        <a href="#">
          <i class="fa fa-users"></i>
          <span> Gestion des Utilisateurs</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <?php if (isset($_SESSION['ADMIN'])) { ?>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url(array('Administration','CandidatureForMod')); ?>"><i class="ion-android-add-circle"></i> Ajouter un Moderateur </a></li>
          <li><a href="<?php echo site_url(array('Administration','ForDemodulation')); ?>"><i class="ion-android-add-circle"></i> Retirer un Moderateur </a></li>
          <li><a href="<?php echo site_url(array('Administration','listBloquerUser')); ?>"><i class="ion-android-add-circle"></i> Bloquer un utilisateur </a></li>
          <li><a href="<?php echo site_url(array('Administration','listDebloquerUser')); ?>"><i class="ion-android-add-circle"></i> Activer un utilisateur </a></li>
        </ul>
         <?php }else { ?>

        <?php } ?>
      </li>      
      <?php } ?>
      <li class="treeview  menu-open">
        <a href="#">
          <i class="fa fa-th "></i>
          <span>Gestion des Categorie</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <?php if (isset($_SESSION['ADMIN'])) { ?>
            <li style="<?php if (isset($_SESSION['ADMIN'])) { echo ("display: block;");  } else{ echo ("display: none;");  } ?>"><a href="<?php echo site_url(array('Administration','formulaireAddCat')) ?>" data-toggle="modal" ><i class="ion-android-add-circle"></i>  Ajouter une Categorie </a></li>
            <li style="<?php if (isset($_SESSION['ADMIN'])) { echo ("display: block;");  } else{ echo ("display: none;");  } ?>"><a href="<?php echo site_url(array('Administration','listeCategorie')); ?>"><i class="fa fa-star"></i> Liste des Categorie </a></li>
          <?php } else { ?>
            <li style="<?php if (isset($_SESSION['Moderateur'])) { echo ("display: block;");  } else{ echo ("display: none;");  } ?>"><a href="<?php echo site_url(array('Moderateur','formAddCat')) ?>" data-toggle="modal" ><i class="ion-android-add-circle"></i>  Ajouter une Categorie </a></li>
            <li style="<?php if (isset($_SESSION['Moderateur'])) { echo ("display: block;");  } else{ echo ("display: none;");  } ?>"><a href="<?php echo site_url(array('Moderateur','Categorielist')); ?>"><i class="fa fa-star"></i> Liste des Categorie </a></li>
          <?php } ?>
        </ul>
      </li>
      <li class="treeview  menu-open">
        <a href="#">
          <i class="fa fa-list"></i>
          <span>Gestion des Themes</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <?php if (isset($_SESSION['ADMIN'])) {?>
          <li><a href="<?php echo site_url(array('Administration','formulaireAddTheme')); ?>" data-toggle="modal" ><i class="ion-android-add-circle"></i>  Ajouter un Theme </a></li>
          <li><a href="<?php echo site_url(array('Administration','liste_Theme')); ?>"><i class="fa fa-star"></i> Liste des Themes </a></li>
        <?php }else{?>
          <li><a href="<?php echo site_url(array('Moderateur','formulaireAddTheme')); ?>" data-toggle="modal" ><i class="ion-android-add-circle"></i>  Ajouter un Theme </a></li>
          <li><a href="<?php echo site_url(array('Moderateur','liste_Theme')); ?>"><i class="fa fa-star"></i> Liste des Themes </a></li>
        <?php }?>
        </ul>
      </li>
      <li class="  ">
        <a href="<?php echo site_url(array('Welcome','index')); ?>">
          <i class="fa fa-eye"></i>
          <span>Voir le Forum</span> 
        </a>
     </li>
      <li style="<?php if (isset($_SESSION['ADMIN'])) { echo ("display: block;");  } else{ echo ("display: none;");  } ?>"><a href="<?php echo site_url(array('Administration','deconnexion')); ?>"><i class="fa fa-power-off text-red"></i> <span> Deconnexion </span></a></li>
      <li style="<?php if (isset($_SESSION['Moderateur'])) { echo ("display: block;");  } else{ echo ("display: none;");  } ?>"><a href="<?php echo site_url(array('Moderateur','deconnexion')); ?>"><i class="fa fa-power-off text-red"></i> <span> Deconnexion </span></a></li>
  </section>

</aside>

  
