<?php echo css('forum-style') ?>

<style>
	
	/*.cl{
	width: 100px;
	height: 100px;
}

.bouttom{
	color: blue;
	border: none;
	background-color: inherit !important;
}*/
</style>	
 <div class="content-wrapper">
 		<section class="content-header">	
      <h1>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(array('Administration','index')); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Gestion des Categories</a></li>
        <li class="active">Liste des Categorie</li>
      </ol>
	    <div class="col-md-offset-1 col-md-8 " style="text-align: center;">
				<h2>LISTE DE TOUT LES THEMES EXISTANT</h2>
			</div>
			<div class="col-md-2" style="margin-top:15px;">
			 <?php 	if (isset($_SESSION['ADMIN'])) { ?>
				 <a href="<?php echo site_url(array('Administration','formulaireAddTheme')) ?>" class="btn btn-success">
	 			<span class="glyphicon glyphicon-plus"></span>ajouter un Theme</a>
			<?php }else{ ?> 
				<a href="<?php echo site_url(array('Moderateur','formulaireAddTheme')) ?>" class="btn btn-success"> <span class="glyphicon glyphicon-plus"></span>ajouter un Theme</a>
			<?php 	} ?>
	 
			</div>
		</section>
    <section class="content">
			<table class=" dataTables_filter table-responsive table-bordered" id="myTable">
				<thead >
					<th>&nbsp</th>
					<th  >Libelle</th>	
					<th  >nbr_commentaire</th>	
					<th  >Date-creation</th>
					<th  >Categorie</th>
					<th  >Auteur</th>
					<th style="<?php if (isset($_SESSION['Moderateur'])) {
						echo 'display: block';
					} ?>" >action</th>
				</thead>	
				<tbody>
          <?php for ($i=0; $i<$themes['total'];$i++) { ?> 
						<tr>
							<td class="text-center"><?php echo $i ?></td>			
							<td class="text-center"><?php echo $themes[$i]['libelle']?> </td>	

							

							<?php if (isset($_SESSION['ADMIN'])) { ?>
								<td  class="text-center">
									<form action="<?php echo site_url(array('Administration','commentaire_theme')) ?>" method="post">
										<input type="hidden" value="<?php echo $themes[$i]['id'] ?>" name="id_theme">
										<input type="hidden" value="<?php echo $themes[$i]['categorie'] ?>" name="categorie">
										<input type="hidden" value="<?php echo $themes[$i]['libelle'] ?>" name="theme">
										<button class="boutton" type="submit"><?php $a=0; if($comment[$i]['total']<10){echo $a.$comment[$i]['total'];}else{echo $comment[$i]['total'];} ?></button>
									</form>
								</td>
							<?php }else{ ?> 

									<td class="text-center">  
										<form action="<?php echo site_url(array('Moderateur','commentaire_theme')) ?>" method="post">
											<input type="hidden" value="<?php echo $themes[$i]['id'] ?>" name="id_theme">
											<input type="hidden" value="<?php echo $themes[$i]['categorie'] ?>" name="categorie">
											<input type="hidden" value="<?php echo $themes[$i]['libelle'] ?>" name="theme">
											<button class="boutton" type="submit" class="boutton"><?php $a=0; if($comment[$i]['total']<10){echo $a.$comment[$i]['total'];}else{echo $comment[$i]['total'];} ?></button>
										</form>
									</td>
							<?php } ?>
							<td class="text-center"><?php echo $themes[$i]['date_creation']?> </td>	
							<td  class="text-center"><?php echo $themes[$i]['categorie'];   ?> </td> 
							<td class="text-center" ><?php echo $themes[$i]['nom'];   ?> </td> 



							<!-- Actions de l administrateur -->
							<td class="text-center" style="<?php if (isset($_SESSION['Moderateur'])) {
						       echo 'display: none';
					         } ?>">
							 	<form action="<?php echo site_url(array('Administration','supprimerTheme')) ?>" method="post" style="display: inline-block;">
							 	  <input type="hidden" value="<?php echo $themes[$i]['id'] ?>" name="id">
							 		<?php if ($comment[$i]['total']==0){ ?>
							 	   <button class="boutton" type="submit"  ><i class="fa fa-trash"></i> </button>
							 		<?php }else{?>
							 			<button class="boutton" type="submit" disabled="true" ><i class="fa fa-trash"></i> </button>
							 		<?php } ?>
							 			 
							 	</form>
							 	<form action="<?php echo site_url(array('Administration','formcommenterTheme')) ?>" method="post" style="display: inline-block;">
							 		<input type="hidden" value="<?php echo $themes[$i]['id'] ?>" name="id">
							 		<input type="hidden" value="<?php echo $themes[$i]['libelle'] ?>" name="libelle">
							 		<input type="hidden" value="<?php echo $_SESSION['ADMIN']['id'] ?>" name="id_user">
							 		<button class="boutton" type="submit"><i class="fa fa-comment"></i> </button>
							 	</form>
							 	<form action="<?php echo site_url(array('Administration','formodifiertheme')) ?>" style="display:inline-block;" method="post">
							 		<input type="hidden" value="<?php echo $themes[$i]['id'] ?>" name="id_theme">
							 		<input type="hidden" value="<?php echo $themes[$i]['libelle'] ?>" name="lib">
							 		<?php if ($comment[$i]['total']==0){ ?>
							 		 <button class="boutton" type="submit" title="Editer" ><i class="fa fa-edit"></i> </button>
							 		<?php }else{?>
							 			<button class="boutton" type="submit" disabled="true" title="Editer"><i class="fa fa-edit"></i> </button>
							 		<?php } ?>
							 	</form>
							</td> 


							<!-- actions du moderateur -->
							<td class="text-center" style="<?php if (isset($_SESSION['ADMIN'])) {
						       echo 'display: none';
					         } ?>">
							 	<form action="<?php echo site_url(array('Moderateur','supprimerTheme')) ?>" method="post" style="display: inline-block;">  
							 	 <input type="hidden" value="<?php echo $themes[$i]['id'] ?>" name="id"> 
							 		<?php if ($comment[$i]['total']==0){ ?>
							 	   <button class="boutton" type="submit"  ><i class="fa fa-trash"></i> </button> 
							 		<?php }else{?>
							 			 <button class="boutton" type="submit" disabled="true" ><i class="fa fa-trash"></i> </button> 
							 		<?php } ?>
							 			 
							 	</form>
							 	<form action="<?php echo site_url(array('Moderateur','formcommenterTheme')) ?>" method="post" style="display: inline-block;">
							 		<input type="hidden" value="<?php echo $themes[$i]['id'] ?>" name="id">
							 		<input type="hidden" value="<?php echo $themes[$i]['libelle'] ?>" name="libelle">
							 		<input type="hidden" value="<?php echo $_SESSION['Moderateur']['id'] ?>" name="id_user">
							 		<button class="boutton" type="submit"><i class="fa fa-comment"></i> </button>
							 	</form>
							 	<form action="<?php echo site_url(array('Moderateur','formodifiertheme')) ?>" style="display:inline-block;" method="post">
							 		<input type="hidden" value="<?php echo $themes[$i]['id'] ?>" name="id_theme">
							 		<input type="hidden" value="<?php echo $themes[$i]['libelle'] ?>" name="lib">
							 		<?php if ($comment[$i]['total']==0){ ?>
							 		 <button class="boutton" type="submit" title="Editer" ><i class="fa fa-edit"></i> </button>
							 		<?php }else{?>
							 			<button class="boutton" type="submit" disabled="true" title="Editer"><i class="fa fa-edit"></i> </button>
							 		<?php } ?>
							 	</form>
							</td>
						</tr>
				<?php }  ?>
				</tbody>	
			</table>
			
		</section>	
</div>
<?php 	echo js('app'); ?>
<?php echo js('jquery-3.6.0.min'); ?>