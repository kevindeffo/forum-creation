<div class="content-wrapper">
	<ol class="breadcrumb pull pull-right">
        <li><a href="<?php echo site_url(array('Administration','index')) ?>"><i class="fa fa-dashboard"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Home</font></font></a></li>
        <li class="active"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Notifications des Thèmes</font></font></li>
    </ol>
	<table id="myTable" class="dataTables_filter table-responsive two-axis">
		<thead style="text-align:center; background-color:red; color:white;">
			<!-- <th>Catégorie</th>
			<th>Auteur</th>
			<th>Contenu</th>
			<th>date_creation</th>
			<th>date_modification</th> -->
		</thead>
		<tbody>
			<?php 
				if ($allnotiftheme['data']=='ok'){
					for ($i=0; $i <$allnotiftheme['total']; $i++){?>
			<tr style="text-align:center;">
				<td><?php echo $allnotiftheme[$i]['id_cat']; ?></td>
				<td><strong><?php $a=$allnotiftheme[$i]['id_user'];
				$cord=$this->User->finduserInfos($a);
				echo $cord['nom']." ".$cord['prenom']; ?></strong></td>
				<td>vient de créer un nouveau <strong style="color:red;">Thème</strong> intitulé: <a href="#"><?php echo $allnotiftheme[$i]['libelle'];?></a></td>
				<td><?php echo $allnotiftheme[$i]['date_creation']; ?></td>
				<td>RAS</td>
			</tr>
			<?php }}else{} ?>
		</tbody>
	</table>
</div>