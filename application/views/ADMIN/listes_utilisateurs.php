<div class="content-wrapper">
    <section class="content-header">
      <table id="mytable" class="dataTables_filter table-responsive"> 
          <thead> 
              <th>N°: </th>
              <th> Image </th>
              <th> Nom </th>
              <th> Prenom </th>
              <th> Status </th>
              <th> Email</th>
              <th> Action</th>
          </thead>
          <tbody> 
              <?php   
               if ($alluser['data']=='ok') {
                    for($i=0; $i<$alluser['total']; $i++) {?>
              <tr>  
                  <td> <?php  echo $alluser[$i]['id']; ?> </td>
                  <td> <?php echo $alluser[$i]['profil'];  ?> </td>
                  <td> <?php  echo $alluser[$i]['nom']; ?></td>
                  <td> <?php  echo $alluser[$i]['prenom']; ?></td>
                  <?php if($alluser[$i]['niveau']==1){ ?>
                          <td> Abonne </td>
                          <td> <?php  echo $alluser[$i]['email']; ?> </td>
                  <?php }elseif($alluser[$i]['niveau']==2){ ?>
                          <td> Modérateur </td>
                          <td> <?php  echo $allmod[$i]['email']; ?> </td>
                  <?php }elseif($alluser[$i]['niveau']==3){ ?>
                          <td> Administrateur </td>
                          <td> <?php echo $allutil[$i]['email'];  ?> </td>
                  <?php } ?>
                  <td>
                    <button><span class="fa fa-filter"></span></button>
                    <button><span class="fa fa fa-trash-o"></span></button>
                  </td>
              </tr>
            <?php } }else{ } ?>
          </tbody>
      </table>
    </section>
  </div>