<?php echo css('forum-style') ?>


<style>
  
  .cl{
  width: 200px;
  height: 200px;
  border-radius: 50%;
}

.bouttom{
  color: blue;
  border: none;
  background-color: inherit !important;

}
</style>
  <div class="content-wrapper" style="background-color:white;">

    <section class="content-header">
      <h1>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php if(isset($_SESSION['ADMIN'])) { echo site_url(array('Administration','index')); } else { echo site_url(array('Moderateur','index')); } ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Gestion des Categories</a></li>
        <li class="active">Liste des Categorie</li>
      </ol>
    </section>
    <section class="content-header">
    <div class="col-md-offset-1 col-md-8 " style="text-align: center; margin-bottom:50px;">
      <h2>MON PROFIL</h2>
    </div>
  </section>
    <section class="content">
      <div class="row">
        <div class="col-md-offset-4 col-md-3" >
          <?php if (isset($_SESSION['ADMIN'])) {?>
            <?php echo imgProfil($_SESSION['ADMIN']['profil'], 'user-image cl','user-image','user-image'); ?>
                  <!-- <img src="<?php //echo img_url('logo.jpg') ?>" class="user-image" alt="User Image" style="height:200px; width: 200px; border-radius:50%;"> -->
                <?php } else{ ?>
                  <?php echo imgProfil($_SESSION['Moderateur']['profil'], 'user-image cl','user-image','user-image'); ?>
                     <!-- <img src="<?php //echo img_url('user_profil/'.$_SESSION['Moderateur']['profil']); ?>" class="user-image" alt="User Image" style="height:200px; width: 200px; border-radius:50%;"> -->
            <?php }  ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-offset-3 col-md-4">
          <?php if (isset($_SESSION['ADMIN'])) {?>                  
                    <h3 style="margin-top:50px; margin-bottom:50px; ">
                     <u> Nom</u>: <?php echo $_SESSION['ADMIN']['nom']?>
                    </h3>
                    <h3 style="margin-top:50px; margin-bottom:50px; ">
                     <u> Prenom</u>: <?php echo $_SESSION['ADMIN']['prenom']?>
                    </h3>
                <?php }else{ ?>
                      <h3 style="margin-top:50px; margin-bottom:50px; ">
                      <u> Nom</u>: <?php echo $_SESSION['Moderateur']['nom']?>
                    </h3>
                    <h3 style="margin-top:50px; margin-bottom:50px; ">
                      <u> Prenom</u>: <?php echo $_SESSION['Moderateur']['prenom']?>
                    </h3>
               <?php } ?>

               <?php if (isset($_SESSION['ADMIN'])) {?>                  
                    <h3 style="margin-top:50px; margin-bottom:50px; ">
                     <u> Email</u>: <?php echo $_SESSION['ADMIN']['email']?>
                    </h3>
                <?php }else{ ?>
                      <h3 style="margin-top:50px; margin-bottom:50px; ">
                      <u> Email</u>: <?php echo $_SESSION['Moderateur']['email']?>
                    </h3>
               <?php } ?>


        </div>
      </div>
      <div class="row">
        <div class="col-md-offset-4 col-md-1">
           <?php if (isset($_SESSION['ADMIN'])) {?> 
          <a href="<?php echo site_url(array('Administration','formmodifierprofil')) ?>" class="btn btn-success" >modifier le profil </a>
            <?php }else{ ?>
          <a href="<?php echo site_url(array('Moderateur','formmodifierprofil')) ?>" class="btn btn-success" >modifier le profil </a>
               <?php } ?>
        </div>
      </div>
          
   </section>
   <?php  
      unset($_SESSION['message_save']);
     ?>
</div>