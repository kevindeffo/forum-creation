  <div class="content-wrapper" style="background-color:white;">

    <section class="content-header">
      <h1>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php if(isset($_SESSION['ADMIN'])) { echo site_url(array('Administration','index')); } else { echo site_url(array('Moderateur','index')); } ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Gestion des Categories</a></li>
        <li class="active">Liste des Categorie</li>
      </ol>
    </section>
    <section class="content-header">
    <div class="col-md-offset-2 col-md-8 " style="text-align: center;">
      <h4 style="margin-top:50px; margin-bottom:50px; ">Entrez vos nouvelles informations afin de modifier votre profil</h4>
    </div>
  </section>
  	<section class="content">
  	<div class="row">
	<div class="col-md-offset-3 col-md-5" style="border:2px solid black;">
    <p><?php if (isset($_SESSION['message_error'])) { echo $_SESSION['message_error'];
    } ?></p>
		<form action="<?php if(isset($_SESSION['ADMIN'])) { echo site_url(array('Administration','modifierprofil')); } else { echo site_url(array('Moderateur','modifierprofil')); } ?>" method="post" enctype="multipart/form-data">
			<input style="margin-top:50px; margin-bottom:50px; " type="text" value="<?php if (isset($_SESSION['ADMIN'])){echo $_SESSION['ADMIN']['nom'];}else{echo $_SESSION['Moderateur']['nom'];} ?>" class="form-control" name="newnom">
			<input type="text" value="<?php if (isset($_SESSION['ADMIN'])){echo $_SESSION['ADMIN']['prenom'];}else{echo $_SESSION['Moderateur']['prenom'];} ?>" class="form-control" name="newprenom">
			<input style="margin-top:50px; margin-bottom:50px; " type="email" value="<?php if (isset($_SESSION['ADMIN'])){echo $_SESSION['ADMIN']['email'];}else{echo $_SESSION['Moderateur']['email'];} ?>" class="form-control" name="newemail">
			<div class="form-group" style="margin-top:30px;">  
            <input id="image" type="file"  name="newprofil" onchange="loadFile(event)" value="<?php if (isset($_SESSION['ADMIN'])){echo $_SESSION['ADMIN']['profil'];}else{echo $_SESSION['Moderateur']['profil'];}  ?>" style="height: 0px;" name="newprofil">
          </div>
          <div class="row">
          <div class="col-md-offset-4 col-md-4 " style="margin-bottom: 20px;" >
            <label for="image">
              <img id="im"  style="width: 100px; height: 100px; cursor: pointer;" src="<?php if (isset($_SESSION['ADMIN'])){echo imgProfil_url($_SESSION['ADMIN']['profil']);}else{echo imgProfil_url($_SESSION['Moderateur']['profil']);} ?>">
            </label>
          </div>
        </div>
         <button class="btn btn-success pull-right" style="margin-bottom: 20px;"><i class="fa fa-check"></i> Enregistrer</button>
         <a href="<?php if(isset($_SESSION['ADMIN'])) { echo site_url(array('Administration','formmodifierprofil')); } else { echo site_url(array('Moderateur','formmodifierprofil')); } ?>" class="btn btn-danger pull-left"><i class="fa fa-times" ></i> annuler</a>
		</form>
	</div>
    </div>
   </section>
   <section class="content">
    </section>
</div>
<script type="text/javascript">
  var loadFile = function(event) {
    var profil = document.getElementById('im');
    profil.src = URL.createObjectURL(event.target.files[0]);
  };
</script>


