<div class="content-wrapper">
	<ol class="breadcrumb pull pull-right">
        <li><a href="<?php echo site_url(array('Administration','index')) ?>"><i class="fa fa-dashboard"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Home</font></font></a></li>
        <li class="active"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Notifications des Catégories</font></font></li>
    </ol>
	<table id="myTable" class="dataTables_filter table-responsive two-axis">
		<thead style="text-align:center; background-color:red; color:white;">
			<!-- <th>Image</th>
			<th>Auteur</th>
			<th>Contenu</th>
			<th>date_creation</th>
			<th>Modification</th> -->
		</thead>
		<tbody>
			<?php 
				if ($allnotifcategorie['data']=='ok'){
					for ($i=0; $i <$allnotifcategorie['total']; $i++){?>
			<tr style="text-align:center;">
				<td><?php echo $allnotifcategorie[$i]['photo_cat']; ?></td>
				<td><?php $a=$allnotifcategorie[$i]['id_user'];
				$cord=$this->User->finduserInfos($a);
				echo $cord['nom']." ".$cord['prenom']; ?></td>
				<td>vient de créer une nouvelle <strong style="color:red;">Catégorie</strong> intitulée: <a href="#"><?php echo $allnotifcategorie[$i]['nom_cat'];?></a></td>
				<td><?php echo $allnotifcategorie[$i]['date_creation']; ?></td>
				<td>RAS</td>
			</tr>
			<?php }}else{} ?>
		</tbody>
	</table>
</div>