

<div class="content-wrapper">

  <section class="content-header">
    <div class="box-header with-border">
      <h3 class="box-title">Modifiez les information de cette categorie</h3>
    </div>
  </section>
  <section class="content">
    <div class="box box-primary col-md-offset-4 col-md-5" style="background-color:white; margin-top: 100px; margin-bottom: 100px; border-radius: 8px;">
      
      <!-- /.box-header -->
      <!-- form start -->
      <form action="<?php if(isset($_SESSION['ADMIN'])) { echo site_url(array('Administration','modifierCategorie')); } else { echo site_url(array('Moderateur','modifCategorie')); } ?>" method="post" enctype="multipart/form-data">
        <div class="box-body">
          <div class="form-group">
            <label >Nouveau titre</label>
            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Entrez le Nouveau titre de le la categorie" name="newnom_cat" value="<?php echo $infocategorie['nom_cat'] ?>">
          </div>
          <input type="hidden" value="<?php echo date('d/m/y h:i:s') ?>" name="date_modification">
          <input type="hidden" value="<?php echo $infocategorie['id'] ?>" name="id_cat">
          <div class="form-group" style="margin-top:30px;"> 
            <p>Entrez une nouvelle image qui represente la categorie</p> 
            <input id="image" type="file"  name="newphoto_cat" onchange="loadFile(event)" value="<?php echo $infocategorie['photo_cat'];  ?>" style="height: 0px;">
          </div> 
          
        </div>
        <div class="row">
          <div class="col-md-offset-4 col-md-4 " style="margin-bottom: 20px;" >
            <label for="image">
              <img id="im"  style="width: 100px; height: 100px; cursor: pointer;" src="<?php echo img_url($infocategorie['photo_cat']) ?>">
            </label>
          </div>
        </div>
        <input type="hidden" name="id_user" value="<?php if(isset($_SESSION['ADMIN'])) { echo $_SESSION['ADMIN']['id_user']; } else { echo $_SESSION['Moderateur']['id_user']; } ?>">
        <input type="hidden" name="niveau" value="1">

        <div class="box-footer" >
          <button type="submit" class="btn btn-primary">Enregistrer</button>
        </div>
      </form>
    </div>
  </section>
</div>
<script type="text/javascript">
  var loadFile = function(event) {
    var profil = document.getElementById('im');
    profil.src = URL.createObjectURL(event.target.files[0]);
  };
</script>
